# vector_haptics

Implementation of our 6-DOF haptic rendering method proposed in the paper "Vectorised High-Fidelity Haptic Rendering with Dynamic Pointshell" submitted for conference publication (2020). The paper was rejected, so this work has not yet been published. An abstract from the manuscript version can be found below.

## Paper abstract

"Exploiting parallelism in haptic rendering algorithms for rigid body collision simulation can be difficult due to the haptic feedback loop imposing strict real-time constraints on the computations. In this paper, we show that the classical Voxmap PointShell algorithm can be efficiently vectorised via the single-program multiple-data (SPMD) programming model of the ISPC programming language and compiler. Our vectorised version provides an average 3x speedup compared to a corresponding scalar implementation, for a static hierarchical pointshell on a single CPU core. In addition, we propose a dynamic pointshell that does not require any pre-processing and allows a fixed point budget to be set per frame. The speedup obtained by the vectorisation means that a larger number of contact queries can be processed per haptic frame, while maintaining a desired haptic framerate. In an empirical study, we demonstrate that this increased fidelity in collision simulation translates directly to a higher user accuracy in assembly of fractured virtual objects."

## Example screenshot

![Screenshot](/screenshot.png?raw=true "Screenshot")

## Compiling

This code has been tested under Ubuntu 16.04 and on Windows 10. Pre-compiled ISPC object files (built with ISPC 1.12) for the vectorisation are included under the `./src/ispc` folder.

To build with GCC on Linux, set the environment variable `VECTOR_HAPTICS_ROOT` to the path where this `README.md` is located, and run the provided build.sh script.

To build with Visual Studio (2013 or later) on Windows, set the `VECTOR_HAPTICS_ROOT` environment variable as above, and use CMake to generate build files.

To run the demo, use the build script or run the binary generated in the `./bin` folder.

## Usage

TODO.

## License

The code is provided under the MIT license (see `LICENSE.md`).
