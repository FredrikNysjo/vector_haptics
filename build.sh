#!/bin/bash

if [ -z "$VECTOR_HAPTICS_ROOT" ]; then
    echo "VECTOR_HAPTICS_ROOT is not set"
    exit 1
fi

if [ -z "$VECTOR_HAPTICS_GENERATOR" ]; then
    VECTOR_HAPTICS_GENERATOR="Unix Makefiles"
    if [[ "$OSTYPE" == "msys" ]]; then
        VECTOR_HAPTICS_GENERATOR="Visual Studio 16 2019"
    fi
fi

# Generate build files
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -G "$VECTOR_HAPTICS_GENERATOR" ../ -DCMAKE_INSTALL_PREFIX=../ && \

# Build and install the program
cmake --build . --config RelWithDebInfo --target install -- && \

# Run the program
cd .. && \
`find ./bin -executable -type f`
