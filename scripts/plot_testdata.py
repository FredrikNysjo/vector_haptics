import numpy as np
import scipy as sp
import scipy.stats
import matplotlib.pyplot as plt
import seaborn as sns

data = [np.loadtxt('input/test_user4.txt'),
        np.loadtxt('input/test_user5.txt'),
        np.loadtxt('input/test_user6.txt'),
        np.loadtxt('input/test_user7.txt'),
        np.loadtxt('input/test_user8.txt')]
colors = ['steelblue', 'darkorange', 'forestgreen', 'crimson', 'mediumpurple']

fig1 = plt.figure()
n_smaller, n_total = 0, 0
s, v = [], []
for i in xrange(0, len(data)):
    for j in xrange(0, 8):
        p0 = data[i][j * 2 + 0]
        p1 = data[i][j * 2 + 1]
        if (p0[1] > p1[1]):
            p0, p1 = p1, p0
        n_total += 1
        n_smaller += abs(p1[5]) < abs(p0[5])
        s.append(abs(p0[5]))
        v.append(abs(p1[5]))
        jitter = np.random.rand() * 0.1 - 0.05
        plt.plot((i + 1) + jitter, abs(p1[5]) - abs(p0[5]), 'o', markersize=9, color=colors[i])
plt.plot([0, 6], [0, 0], '--', color='black')

#plt.title('Dynamic pointshell', fontsize=24)
plt.xticks(range(1,6), ('U1', 'U2', 'U3', 'U4', 'U5'), fontsize=24)
plt.yticks(fontsize=20)
plt.xlim([0, 6])
plt.ylim([-0.25, 0.25])
plt.ylabel('Error difference (in voxels)', fontsize=24)
fig1.tight_layout()
plt.savefig('output/dynamic_ps.png', bbox_inches='tight')
print(n_total, n_smaller, sp.stats.wilcoxon(s, v), sp.stats.describe(s).mean, sp.stats.describe(v).mean)

fig2 = plt.figure()
n_smaller, n_total = 0, 0
s, v = [], []
for i in xrange(0, len(data)):
    for j in xrange(8, 16):
        p0 = data[i][j * 2 + 0]
        p1 = data[i][j * 2 + 1]
        if (p0[1] > p1[1]):
            p0, p1 = p1, p0
        n_total += 1
        n_smaller += abs(p1[5]) < abs(p0[5])
        s.append(abs(p0[5]))
        v.append(abs(p1[5]))
        jitter = np.random.rand() * 0.1 - 0.05
        plt.plot((i + 1) + jitter, abs(p1[5]) - abs(p0[5]), 'o', markersize=9, color=colors[i])
plt.plot([0, 6], [0, 0], '--', color='black')

#plt.title('Static pointshell', fontsize=24)
plt.xticks(range(1,6), ('U1', 'U2', 'U3', 'U4', 'U5'), fontsize=24)
plt.yticks(fontsize=20)
plt.xlim([0, 6])
plt.ylim([-0.25, 0.25])
plt.ylabel('Error difference (in voxels)', fontsize=24)
fig2.tight_layout()
plt.savefig('output/static_ps.png', bbox_inches='tight')
print(n_total, n_smaller, sp.stats.wilcoxon(s, v), sp.stats.describe(s).mean, sp.stats.describe(v).mean)

plt.show()
