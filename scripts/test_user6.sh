#!/bin/bash

# Dynamic pointshell (trial 1)
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 1 0 2048
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 1 1 8192
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 1 1 8192
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 1 0 2048
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 1 1 8192
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 1 0 2048
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 1 0 2048
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 1 1 8192

# Dynamic pointshell (trial 2)
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 1 1 8192
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 1 0 2048
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 1 0 2048
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 1 1 8192
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 1 0 2048
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 1 1 8192
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 1 1 8192
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 1 0 2048

# Static pointshell (trial 1)
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 0 0
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 0 1
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 0 1
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 0 0
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 0 1
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 0 0
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 0 0
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 0 1

# Static pointshell (trial 2)
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 0 1
./bin/vector_haptics data/heightmap_test6.png data/heightmap_test6.png 0 0
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 0 0
./bin/vector_haptics data/heightmap_test7.png data/heightmap_test7.png 0 1
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 0 0
./bin/vector_haptics data/heightmap_test4.png data/heightmap_test4.png 0 1
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 0 1
./bin/vector_haptics data/heightmap_test5.png data/heightmap_test5.png 0 0
