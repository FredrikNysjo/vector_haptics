/// @file    CRenderPass.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "CRenderPass.h"
#include "graphics/COpenGLHeaders.h"

namespace chai3d {

cRenderPass::cRenderPass() {}

cRenderPass::~cRenderPass() {}

void cRenderPass::render()
{
    static GLuint s_vao = 0;
    if (!s_vao) glGenVertexArrays(1, &s_vao);

    float proj[16];
    for (int i = 0; i < 16; ++i)
        proj[i] = float(m_camera->m_projectionMatrix.getData()[i]);

    if (m_input) {
        renderInitialize();
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glViewport(0, 0, m_width, m_height);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_input->m_depthBuffer->getTextureId());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_input->m_imageBuffer->getTextureId());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glUseProgram(m_program->getId());
        glDisable(GL_DEPTH_TEST);
        glUniform1i(0, m_frame++);
        glUniform1f(1, m_plane_focus);
        glUniformMatrix4fv(2, 1, GL_FALSE, &proj[0]);
        glBindVertexArray(s_vao);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
        glUseProgram(0);

        renderFinalize();
    }
}

void cRenderPass::blit()
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, m_fbo);
    glBlitFramebuffer(0, 0, m_width, m_height,
                      0, 0, m_width, m_height,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

} // namespace chai3d
