/// @file    CRenderPass.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "display/CFrameBuffer.h"
#include "shaders/CShaderProgram.h"

namespace chai3d {

class cRenderPass;
typedef std::shared_ptr<cRenderPass> cRenderPassPtr;

class cRenderPass : public cFrameBuffer {
public:
    cRenderPass();
    virtual ~cRenderPass();
    static cRenderPassPtr create() { return std::make_shared<cRenderPass>(); }

    void render();
    void blit();

    int m_frame = 0;
    float m_plane_focus = 0.5f;
    cRenderPassPtr m_input;
    cShaderProgramPtr m_program;
};

} // namespace chai3d
