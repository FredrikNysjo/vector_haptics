/// @file    CTrackball.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "CTrackball.h"
#include "math/CQuaternion.h"

#include <cassert>
#include <algorithm>

namespace chai3d {

void cTrackballMove(cTrackball *trackball, double x, double y)
{
    assert(trackball != nullptr);

    if (!trackball->tracking) { return; }

    double speed = trackball->speed;
    double clamp = trackball->clamp;
    double *center = &trackball->center[0];
    cQuaternion *quat = (cQuaternion *)&trackball->quat[0];
    if (quat->length() == 0.0) { *quat = cQuaternion(1.0, 0.0, 0.0, 0.0); }

    double motion_x = x - center[0];
    double motion_y = y - center[1];
    if (std::abs(motion_x) < 1.0 && std::abs(motion_y) < 1.0) { return; }
    center[0] = x, center[1] = y;

    double theta_x = speed * std::max(-clamp, std::min(clamp, motion_x));
    double theta_y = speed * std::max(-clamp, std::min(clamp, motion_y));
    cQuaternion delta_x, delta_y;
    delta_x.fromAxisAngle(cVector3d(0.0, 0.0, 1.0), theta_x);
    delta_y.fromAxisAngle(cVector3d(0.0, 1.0, 0.0), theta_y);

    // Update orientation s.t. quaternion is in positive hemisphere
    *quat = delta_y * delta_x * (*quat);
    quat->normalize();
    if (quat->w < 0.0) { quat->negate(); }
}

void cTrackballGetMatrix3d(const cTrackball *trackball, double *m)
{
    assert(trackball != nullptr);

    const cQuaternion *quat = (const cQuaternion *)&trackball->quat[0];
    quat->toRotMat(*(cMatrix3d *)m);
}

} // namespace chai3d
