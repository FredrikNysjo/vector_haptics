/// @file    CTrackball.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include <math/CVector3d.h>

#include "vh_transform.h"

namespace chai3d {

struct cTrackball {
    double center[2];
    double quat[4];
    double speed = 0.004;
    double clamp = 100.0;
    bool tracking = false;
};

struct cMouseTransform {
    double center[2];
    vh::Vec3 pos[2];
    double speed = 0.004;
    bool moving = false;
};

struct cGrabTransform {
    cVector3d pos;
    bool moving = false;
};

void cTrackballMove(cTrackball *trackball, double x, double y);

void cTrackballGetMatrix3d(const cTrackball *trackball, double *m);

} // namespace chai3d
