/// @file    CVolumeObject.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "CVolumeObject.h"
#include <shaders/CShaderProgram.h>

#include "vh_context.h"
#include "vh_volume.h"
#include "vh_pointshell.h"

#include <cassert>

namespace chai3d {

cVolumeObject::cVolumeObject()
{
    m_programs[0] = cShaderProgram::create(
        #include "glsl/raycast.vert"
        ,
        #include "glsl/raycast.frag"
    );
    m_programs[1] = cShaderProgram::create(
        #include "glsl/grid.vert"
        ,
        #include "glsl/grid.frag"
    );
    m_programs[2] = cShaderProgram::create(
        #include "glsl/pointshell.vert"
        ,
        #include "glsl/pointshell.frag"
    );

    m_showBoundaryBox = false;
    updateBoundaryBox();
}

cVolumeObject::~cVolumeObject()
{
}

void cVolumeObject::render(cRenderOptions &options)
{
    if (!m_vtc) { return; }

    if (!m_volume_texture)
        vh::createVolumeTexture(&m_vtc->voxmaps[m_id], &m_volume_texture);
    if (!m_minmax_volume_texture)
        vh::createVolumeTexture(&m_vtc->minmax_volumes[m_id], &m_minmax_volume_texture);
    if (!m_mask_volume_texture)
        vh::createVolumeTexture(&m_vtc->mask_volumes[m_id], &m_mask_volume_texture);

    this->renderVolume(options);
    this->renderPointShell(options);
}

void cVolumeObject::updateBoundaryBox()
{
    if (!m_vtc) { return; }

    const auto &aabb = m_vtc->bounds[m_id];
    m_boundaryBoxMin.set(aabb[0].x, aabb[0].y, aabb[0].z);
    m_boundaryBoxMax.set(aabb[1].x, aabb[1].y, aabb[1].z);
    m_extent[0] = aabb[1].x - aabb[0].x;
    m_extent[1] = aabb[1].y - aabb[0].y;
    m_extent[2] = aabb[1].z - aabb[0].z;
}

void cVolumeObject::renderVolume(cRenderOptions &options)
{
    static GLuint s_vao = 0;
    if (!s_vao) glGenVertexArrays(1, &s_vao);

    if (m_show_isosurface || m_show_minmax_blocks || m_show_proxy_geometry) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_3D, m_minmax_volume_texture);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, m_volume_texture);

        glUseProgram(m_programs[0]->getId());
        glUniform3fv(0, 1, &m_extent[0]);
        glUniform1f(1, m_vtc->isovalues[m_id]);
        glUniform1i(2, m_show_proxy_geometry);
        glUniform1i(3, m_show_minmax_blocks);
        glUniform1i(4, m_show_label ? m_label : -1);
        glBindVertexArray(s_vao);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);
        glBindVertexArray(0);
        glUseProgram(0);
    }

    if (m_show_collision_grid || m_show_coarse_collisions) {
        const auto &mask_volume = m_vtc->mask_volumes[m_id];
        const uint8_t *mask_data = &mask_volume.data[0];
        const uint32_t *mask_dim = &mask_volume.dimensions[0];
        glBindTexture(GL_TEXTURE_3D, m_mask_volume_texture);
        glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, mask_dim[0], mask_dim[1], mask_dim[2],
                        GL_RED, GL_UNSIGNED_BYTE, mask_data);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_3D, m_mask_volume_texture);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_3D, m_minmax_volume_texture);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, m_volume_texture);

        glUseProgram(m_programs[1]->getId());
        glUniform3fv(0, 1, &m_extent[0]);
        glUniform1f(1, m_vtc->isovalues[m_id]);
        glUniform1i(2, m_show_collision_grid);
        glUniform1i(3, m_show_coarse_collisions);
        glUniform1i(4, m_show_label ? m_label : -1);
        glBindVertexArray(s_vao);
        glDrawArraysInstancedARB(GL_LINE_STRIP, 0, 8, 4096 * 2);
        glBindVertexArray(0);
        glUseProgram(0);
    }
}

void cVolumeObject::renderPointShell(cRenderOptions &options)
{
    static GLuint s_vao = 0;
    if (!s_vao) glGenVertexArrays(1, &s_vao);

    static GLuint s_vbo = 0;
    if (!s_vbo) {
        glGenBuffers(1, &s_vbo);
        glBindVertexArray(s_vao);
        glBindBuffer(GL_ARRAY_BUFFER, s_vbo);
        glBufferData(GL_ARRAY_BUFFER, (1 << 20) * 16, nullptr, GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, 0, 0, nullptr);
        glBindVertexArray(0);
    }

    if (m_show_pointshell) {
        int32_t num_points = 0;
        if (m_pointshell_type == vh::VH_POINTSHELL_TYPE_DYNAMIC) {
            const auto &psi = m_vtc->pointshells[m_id].psi[0];
            uint32_t tail = psi.first;
            uint32_t head = (tail + psi.count) % psi.capacity;
            uint32_t count0 = head > tail ? head - tail : psi.capacity - tail;
            uint32_t count1 = head > tail ? 0 : head;
            glBindBuffer(GL_COPY_WRITE_BUFFER, s_vbo);
            glBufferSubData(GL_COPY_WRITE_BUFFER, 0, psi.capacity * 16, &psi.data[0]);
            num_points = psi.count;
        }
        else {  // m_pointshell_type == VH_POINTSHELL_TYPE_STATIC
            const auto &ps = m_vtc->pointshells2[m_id];
            glBindBuffer(GL_COPY_WRITE_BUFFER, s_vbo);
            glBufferSubData(GL_COPY_WRITE_BUFFER, 0, ps.data.size() * sizeof(ps.data[0]), &ps.data[0]);
            num_points = ps.data.size() / 4;
        }

        glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
        glEnable(GL_POINT_SPRITE);
        glUseProgram(m_programs[2]->getId());
        glUniform3fv(0, 1, &m_extent[0]);
        glUniform1i(1, m_show_point_normals);
        glUniform1i(2, m_show_point_ids);
        glUniform1i(3, m_max_point_lod);
        glUniform1i(4, m_show_label ? m_label : -1);
        glBindVertexArray(s_vao);
        glDrawArrays(GL_POINTS, 0, num_points);
        glBindVertexArray(0);
        glUseProgram(0);
    }
}

} // namespace chai3d
