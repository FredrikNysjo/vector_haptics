/// @file    CVolumeObject.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vh_forward.h"
#include "world/CGenericObject.h"

namespace chai3d {

class cVolumeObject : public cGenericObject {
public:
    cVolumeObject();
    virtual ~cVolumeObject();

    vh::VhContext *m_vtc = nullptr;
    bool m_show_isosurface = true;
    bool m_show_proxy_geometry = false;
    bool m_show_minmax_blocks = false;
    bool m_show_collision_grid = false;
    bool m_show_coarse_collisions = false;
    bool m_show_pointshell = false;
    bool m_show_point_normals = false;
    bool m_show_point_ids = false;
    bool m_show_label = true;
    int32_t m_max_point_lod = 5;
    uint32_t m_pointshell_type = 1;  // TODO
    int32_t m_label = 0;
    int32_t m_id = 0;

    float m_extent[3];
    GLuint m_volume_texture = 0;
    GLuint m_minmax_volume_texture = 0;
    GLuint m_mask_volume_texture = 0;
    cShaderProgramPtr m_programs[3];
protected:
    virtual void render(cRenderOptions &options);
    virtual void updateBoundaryBox();

    void renderVolume(cRenderOptions &options);
    void renderPointShell(cRenderOptions &options);
};

} // namespace chai3d
