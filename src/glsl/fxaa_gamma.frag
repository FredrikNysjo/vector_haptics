R"(
#version 130
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) uniform int u_frame;
layout(location = 2) uniform mat4 u_projection;
layout(binding = 0) uniform sampler2D u_color_tex;
layout(binding = 1) uniform sampler2D u_depth_tex;

in vec2 v_texcoord;
out vec4 rt_color;

float rgb2luma(vec3 color)
{
    vec3 luma = vec3(0.299, 0.587, 0.114);
    return dot(color, luma);
}

vec3 linearToGamma(vec3 color)
{
    return pow(color, vec3(1.0 / 2.2));
}

// Adaption of Timothy Lotte's original FXAA implementation
vec3 fxaa(sampler2D tex, vec2 uv)
{
    const float REDUCE_MIN = 1.0 / 128.0;
    const float REDUCE_MUL = 1.0 / 8.0;
    const float SPAN_MAX = 8.0;

    vec2 uv_shifted = uv - (0.5 / textureSize(tex, 0).xy);
    float lumaNW = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(0, 0)).rgb);
    float lumaNE = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(1, 0)).rgb);
    float lumaSW = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(0, 1)).rgb);
    float lumaSE = rgb2luma(textureLodOffset(tex, uv_shifted, 0, ivec2(1, 1)).rgb);
    float lumaM =  rgb2luma(textureLod(tex, uv, 0).rgb);

    float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
    float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));

    vec2 dir;
    dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
    dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));

    float dirReduce = max(REDUCE_MIN, (lumaNW + lumaNE + lumaSW + lumaSE) * 0.25 * REDUCE_MUL);
    float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);
    dir = clamp(dir * rcpDirMin, -SPAN_MAX, SPAN_MAX) / textureSize(tex, 0).xy;

    vec3 rgbA = 0.5 * (
        textureLod(tex, uv + dir * (1.0 / 3.0 - 0.5), 0).rgb +
        textureLod(tex, uv + dir * (2.0 / 3.0 - 0.5), 0).rgb);
    vec3 rgbB = rgbA * 0.5 + 0.25 * (
        textureLod(tex, uv + dir * (0.0 / 3.0 - 0.5), 0).rgb +
        textureLod(tex, uv + dir * (3.0 / 3.0 - 0.5), 0).rgb);
    float lumaB = rgb2luma(rgbB);
    return ((lumaB < lumaMin) || (lumaB > lumaMax)) ? rgbA : rgbB;
}

void main()
{
    rt_color = vec4(fxaa(u_color_tex, v_texcoord), 1.0);
    rt_color.rgb = linearToGamma(rt_color.rgb);
}
)"
