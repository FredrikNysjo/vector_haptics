R"(
#version 130

in vec3 v_color;
out vec4 rt_color;

void main()
{
    rt_color = vec4(v_color, 1.0);
}
)"
