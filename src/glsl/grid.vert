R"(
#version 130
#extension GL_ARB_draw_instanced : require
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require

#define BLOCKS_PER_SIDE 16

#define u_mvp gl_ModelViewProjectionMatrix
layout(location = 0) uniform vec3 u_extent;
layout(location = 1) uniform float u_isovalue;
layout(location = 2) uniform int u_show_collision_grid;
layout(location = 3) uniform int u_show_coarse_collisions;
layout(location = 4) uniform int u_label;
layout(binding = 0) uniform sampler3D u_volume_tex;
layout(binding = 1) uniform sampler3D u_minmax_volume_tex;
layout(binding = 2) uniform sampler3D u_mask_volume_tex;

const vec3 g_wirecube[] = vec3[](
    vec3(0, 0, 1), vec3(1, 0, 1), vec3(1, 0, 0), vec3(0, 0, 0),
    vec3(0, 0, 1), vec3(0, 1, 1), vec3(1, 1, 1), vec3(1, 0, 1),
    vec3(0, 1, 0), vec3(0, 1, 1), vec3(1, 1, 1), vec3(1, 1, 0),
    vec3(0, 1, 0), vec3(0, 0, 0), vec3(1, 0, 0), vec3(1, 1, 0)
);

vec3 hsv2rgb(float h, float s, float v)
{
    vec3 k = fract(vec3(5.0, 3.0, 1.0) / 6.0 + h) * 6.0;
    return v - v * s * clamp(min(k, 4.0 - k), 0.0, 1.0);
}

out vec3 v_color;

void main()
{
    int idx = gl_InstanceIDARB / 2;
    vec3 p = vec3(idx % 16, (idx / 16) % 16, idx / 256);

    float max_value = texture(u_minmax_volume_tex, (p + 0.5) / 16.0).g;
    float mask = texture(u_mask_volume_tex, (p + 0.5) / 16.0).r;

    gl_Position = vec4(2.0, 2.0, 2.0, 0.0);
    if (max_value <= u_isovalue) return;
    if (!bool(u_show_collision_grid) && mask == 0.0) return;

    int i = gl_InstanceIDARB % 2;
    int j = gl_VertexID % 8;
    vec3 grid_pos = p + g_wirecube[i * 8 + j];
    gl_Position = u_mvp * vec4((grid_pos / 16.0 - 0.5) * u_extent, 1.0);

    v_color = (u_label >= 0) ? hsv2rgb(fract(u_label * 0.618034), 0.6, 1.0) : vec3(0.8);
    v_color *= v_color;  // Compensate for gamma correction

    if (bool(u_show_coarse_collisions) && mask > 0.0) {
        v_color = mix(vec3(1.0), v_color, 0.5);
        // Apply small depth offset to better show intersecting blocks
        gl_Position.z -= 1e-5 / gl_Position.w;
    }
}
)"
