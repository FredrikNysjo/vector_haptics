R"(
#version 130
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require

in vec3 v_color;
out vec4 rt_color;

void main()
{
    if (length(gl_PointCoord - 0.5) > 0.5) discard;
    rt_color = vec4(v_color, 1.0);
}
)"
