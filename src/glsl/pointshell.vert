R"(
#version 130
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require
#extension GL_ARB_shader_bit_encoding : require

#define u_mvp gl_ModelViewProjectionMatrix
#define u_mv gl_ModelViewMatrix
#define u_modelview_inv gl_ModelViewMatrixInverse
layout(location = 0) uniform vec3 u_extent;
layout(location = 1) uniform int u_show_point_normals;
layout(location = 2) uniform int u_show_point_ids;
layout(location = 3) uniform int u_max_lod;
layout(location = 4) uniform int u_label;

layout(location = 0) in vec4 a_point;
out vec3 v_color;

vec3 hsv2rgb(float h, float s, float v)
{
    vec3 k = fract(vec3(5.0, 3.0, 1.0) / 6.0 + h) * 6.0;
    return v - v * s * clamp(min(k, 4.0 - k), 0.0, 1.0);
}

void main()
{
    vec3 local_pos = a_point.xyz;
    local_pos += 0.5 / 16.0;  // FIXME Align with visual isosurface
    local_pos = (local_pos / 16.0) - 0.5;

    int normal_lod = floatBitsToInt(a_point.w);
    vec3 local_normal = vec3(
        float((normal_lod >> 0) & 255) / 127.5 - 1.0,
        float((normal_lod >> 8) & 255) / 127.5 - 1.0,
        float((normal_lod >> 16) & 255) / 127.5 - 1.0);
    int lod = (normal_lod >> 24) & 255;

    vec3 N = normalize(mat3(u_mv) * local_normal);
    vec3 L = normalize(vec3(1.0, 2.0, 3.0));
    vec3 albedo = (u_label >= 0) ? hsv2rgb(fract(u_label * 0.618034), 0.8, 0.9) : vec3(0.8);

    v_color = vec3(0.0);
    v_color += albedo * 0.8 * max(0.0, dot(N, L));
    v_color += albedo * 0.4 * (N.y * 0.4 + 0.6);
    if (bool(u_show_point_normals))
        v_color = local_normal * 0.5 + 0.5;
    if (bool(u_show_point_ids)) {
        v_color = hsv2rgb(float(gl_VertexID) / 4096.0, 0.8, 0.9);
        v_color *= v_color;
    }

    gl_Position = u_mvp * vec4(local_pos * u_extent, 1.0);
    gl_Position.z -= 1e-3 * gl_Position.w;  // Add depth bias
    gl_PointSize = 5.0;

    if (lod > u_max_lod) gl_Position = vec4(2.0, 2.0, 2.0, 0.0);
}
)"
