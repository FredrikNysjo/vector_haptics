R"(
#version 130
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require

#define BLOCKS_PER_SIDE 16
#define EPSILON 1e-5
#define HALF_EPSILON (0.5 * EPSILON)
#define STEP_LENGTH (1.0 / 256.0)
#define USE_JITTER 1

#define u_mvp gl_ModelViewProjectionMatrix
#define u_mv gl_ModelViewMatrix
layout(location = 0) uniform vec3 u_extent;
layout(location = 1) uniform float u_isovalue;
layout(location = 2) uniform int u_show_proxy_geometry;
layout(location = 3) uniform int u_show_minmax_blocks;
layout(location = 4) uniform int u_label;
layout(binding = 0) uniform sampler3D u_volume_tex;
layout(binding = 1) uniform sampler3D u_minmax_volume_tex;

in vec3 v_ray_origin;
in vec3 v_ray_dir;
out vec4 rt_color;

// Branchless ray/box intersection test adapted from
// https://tavianator.com/fast-branchless-raybounding-box-intersections/
bool intersectBox(vec3 ray_origin, vec3 ray_dir_inv, out float tmin, out float tmax)
{
    vec3 t1 = (-(0.5 + HALF_EPSILON) - ray_origin) * ray_dir_inv;
    vec3 t2 = ((0.5 + HALF_EPSILON) - ray_origin) * ray_dir_inv;
    tmin = max(min(t1[0], t2[0]), max(min(t1[1], t2[1]), min(t1[2], t2[2])));
    tmax = min(max(t1[0], t2[0]), min(max(t1[1], t2[1]), max(t1[2], t2[2])));
    return (tmax - tmin) > 0.0;
}

bool raymarch(vec3 ray_origin, vec3 ray_dir, float tmin, float tmax, out vec3 p)
{
    //float rnd = fract(sin(dot(vec2(12.1, 7.3), gl_FragCoord.xy)) * 43758.5453);
    float rnd = fract(dot(vec2(0.754877, 0.569840), gl_FragCoord.xy));

    float t = bool(USE_JITTER) ? tmin + STEP_LENGTH * rnd : tmin;
    while (t < tmax) {
        p = ray_origin + t * ray_dir;
        if (texture(u_volume_tex, p).r > u_isovalue) { break; }
        t += STEP_LENGTH;
    }
    return t < tmax;
}

vec3 imageGrad(vec3 p)
{
    vec3 grad = vec3(0.0);
    grad.x += textureOffset(u_volume_tex, p, ivec3( 1, 0, 0)).r;
    grad.x -= textureOffset(u_volume_tex, p, ivec3(-1, 0, 0)).r;
    grad.y += textureOffset(u_volume_tex, p, ivec3( 0, 1, 0)).r;
    grad.y -= textureOffset(u_volume_tex, p, ivec3( 0,-1, 0)).r;
    grad.z += textureOffset(u_volume_tex, p, ivec3( 0, 0, 1)).r;
    grad.z -= textureOffset(u_volume_tex, p, ivec3( 0, 0,-1)).r;
    return grad;
}

vec3 hsv2rgb(float h, float s, float v)
{
    vec3 k = fract(vec3(5.0, 3.0, 1.0) / 6.0 + h) * 6.0;
    return v - v * s * clamp(min(k, 4.0 - k), 0.0, 1.0);
}

vec3 computeLighting(vec3 p)
{
    vec3 V = -normalize(vec3(u_mv * vec4((p - 0.5) * u_extent, 1.0)));
    vec3 N = -normalize(mat3(u_mv) * imageGrad(p));
    vec3 L = normalize(vec3(1.0, 2.0, 3.0));
    vec3 H = normalize(L + V);
    float F_schlick = (0.04 + (1.0 - 0.04) * pow(1.0 - max(0.0, dot(N, V)), 5.0));
    vec3 albedo = (u_label >= 0) ? hsv2rgb(fract(u_label * 0.618034), 0.7, 0.8) : vec3(0.8);
    albedo *= albedo;  // Compensate for gamma correction

    vec3 color = vec3(0.0);
    color += albedo * 0.8 * max(0.0, dot(N, L));
    color += 0.68 * 0.8 * pow(max(0.0, dot(N, H)), 128.0);
    color += albedo * 0.4 * (N.y * 0.5 + 0.5);
    color += F_schlick * 0.4 * smoothstep(-0.3, 0.3, reflect(N, -V).y);
    return color;
}

void main()
{
    if (!gl_FrontFacing) { discard; }

    vec3 ro = v_ray_origin;
    vec3 rd = normalize(v_ray_dir);
    vec3 rd_inv = clamp(1.0 / rd, -9999.0, 9999.0);
    float tmin, tmax;
    bool hit = intersectBox(ro, rd_inv, tmin, tmax);

    vec4 color = vec4(0.0);
    if (hit) {
        vec3 p = ro + (tmin + EPSILON) * rd + 0.5;
        vec3 pmax = ro + (tmax + EPSILON) * rd + 0.5;
        vec3 q = fract(p * BLOCKS_PER_SIDE) - 0.5;

        while (intersectBox(q, rd_inv, tmin, tmax) && hit && !bool(u_show_proxy_geometry)) {
            if (texture(u_minmax_volume_tex, p).g > u_isovalue) { break; }

            p += (tmax + EPSILON) * (1.0 / BLOCKS_PER_SIDE) * rd;
            q = fract(p * BLOCKS_PER_SIDE) - 0.5;
            hit = all(lessThan(abs(p - 0.5), vec3(0.5)));
        }

        if (bool(u_show_proxy_geometry) || bool(u_show_minmax_blocks)) {
            color = vec4(p, float(hit));
        }
        else {
            hit = raymarch(p, rd, 0.0, length(pmax - p), p);
            if (hit) { color = vec4(computeLighting(p), 1.0); }
        }

        vec4 clip_pos = u_mvp * vec4((p - 0.5) * u_extent, 1.0);
        gl_FragDepth = (clip_pos.z / clip_pos.w) * 0.5 + 0.5;
    }

    if (color.a == 0.0) discard;
    rt_color = color;
}
)"
