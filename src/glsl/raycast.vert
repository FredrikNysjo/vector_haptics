R"(
#version 130
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require

#define u_mvp gl_ModelViewProjectionMatrix
#define u_modelview_inv gl_ModelViewMatrixInverse
layout(location = 0) uniform vec3 u_extent;

out vec3 v_ray_origin;
out vec3 v_ray_dir;

// From "Optimizing Triangle Strips for Fast Rendering" by Evans et al.
const int g_cube_indices[] = int[](
    3, 2, 7, 6, 4, 2, 0, 3, 1, 7, 5, 4, 1, 0
);

void main()
{
    int idx = g_cube_indices[gl_VertexID];
    vec3 cube_pos = vec3(idx % 2, (idx / 2) % 2, idx / 4) - 0.5;

    v_ray_origin = vec3(u_modelview_inv[3]) / u_extent;
    v_ray_dir = cube_pos - v_ray_origin;
    gl_Position = u_mvp * vec4(cube_pos * u_extent, 1.0);
}
)"
