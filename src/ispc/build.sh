#!/bin/bash

if [ -z "$ISPC_EXECUTABLE" ]; then
    echo "ISPC_EXECUTABLE is not set"
    ISPC_EXECUTABLE="$HOME/Software/ispc-v1.12.0-linux/bin/ispc"
    if [[ "$OSTYPE" == "msys" ]]; then
        ISPC_EXECUTABLE="$HOME/Software/ispc-v1.12.0-windows/bin/ispc"
    fi
    echo "Using default path $ISPC_EXECUTABLE"
fi

OUTPUT_DIR="linux64"
if [[ "$OSTYPE" == "msys" ]]; then
    OUTPUT_DIR="win64"
fi

$ISPC_EXECUTABLE -O2 --pic --target=avx2-i32x16 -o collision_ispc.o -h collision_ispc.h collision.ispc && \
mv collision_ispc.o $OUTPUT_DIR
