/// @file    main.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

// Notes:
// - [X] Find out how CHAI3D handles pointer ownership
// - [ ] ...

#include <chai3d.h>
#include "chai3d/CRenderPass.h"
#include "chai3d/CVolumeObject.h"
#include "chai3d/CTrackball.h"

#include "vh_context.h"
#include "vh_collision.h"
#include "vh_pointshell.h"
#include "vh_timing.h"
#include "vh_vtk_utils.h"
#include "ispc/collision_ispc.h"

#include <GLFW/glfw3.h>
#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#define UPDATE_SIMULATION_IN_GFX_THREAD 0

using namespace chai3d;

struct Context {
    cWorld *world;
    cCamera *camera;
    cSpotLight *light;
    cTrackball trackball;
    cGrabTransform grab_transform;
    cMouseTransform mouse_transform;
    cVolumeObject *volume_objects[VH_MAX_ITEMS];
    bool show_volume_object = true;
    bool show_boundary_box = false;
    bool show_isosurface = true;
    bool show_proxy_geometry = false;
    bool show_minmax_blocks = false;
    bool show_collision_grid = false;
    bool show_coarse_collisions = false;
    bool show_pointshell = false;
    bool show_point_normals = false;
    bool show_point_ids = false;

    cHapticDeviceHandler *handler;
    cGenericHapticDevicePtr hapticDevice;
    cToolCursor *cursor;
    cFrequencyCounter freqCounterGraphics;
    cFrequencyCounter freqCounterHaptics;
    cThread *hapticsThread;
    bool simulationRunning = false;
    bool simulationFinished = false;
    float maxStiffness = 100.0f;
    float max_force = 9.0f;
    bool haptics_enabled = true;

    cRenderPassPtr forwardPass;
    cRenderPassPtr fxaaGammaPass;
    bool shadows = false;
    bool fxaa = true;
    bool gamma = true;

    vh::VhContext vtc;
    vh::ForceTorque ft;
    vh::TimingGraph timings[3];
    vh::ComputeMode compute_mode = vh::VH_COMPUTE_MODE_ISPC;
    vh::PointShellType ps_type = vh::VH_POINTSHELL_TYPE_DYNAMIC;
    bool use_collision_mask = true;
    bool use_virtual_coupling = true;
    bool use_force_scaling = false;
    bool use_automatic_lod = true;
    int32_t max_point_lod = 2;
    int32_t max_dynamic_points = 4096;
    std::string input_filenames[2];
    int32_t custom_label = -1;
    uint32_t num_point_queries = 0;

    GLFWwindow *window = nullptr;
    int width  = 0;
    int height = 0;
    int swap_interval = 1;
    bool mirrored = false;
    bool fullscreen = false;
    cStereoMode stereo_mode = C_STEREO_DISABLED;
};

// Forward declarations
void updateHaptics(void *userdata);

void initScene(Context &ctx)
{
    ctx.world = new cWorld();
    ctx.world->m_backgroundColor = { 0.2f, 0.2f, 0.2f };

    ctx.cursor = new cToolCursor(ctx.world);
    ctx.world->addChild(ctx.cursor);
    ctx.cursor->setRadius(0.01);
    ctx.cursor->setHapticDevice(ctx.hapticDevice);
    ctx.cursor->setWorkspaceRadius(0.15);
    ctx.cursor->setWaitForSmallForce(true);
    ctx.cursor->start();

    // Compute workspace-adjusted maximum stiffness of the haptic device
    cHapticDeviceInfo info = ctx.hapticDevice->getSpecifications();
    ctx.maxStiffness = float(info.m_maxLinearStiffness / ctx.cursor->getWorkspaceScaleFactor());

    ctx.camera = new cCamera(ctx.world);
    ctx.world->addChild(ctx.camera);
    ctx.camera->set( cVector3d(0.5, 0.0, 0.0),    // camera position (eye)
                     cVector3d(0.0, 0.0, 0.0),    // look at position (target)
                     cVector3d(0.0, 0.0, 1.0));   // direction of the (up) vector
    ctx.camera->setClippingPlanes(0.1, 5.0);
    ctx.camera->setStereoMode(ctx.stereo_mode);
    ctx.camera->setStereoEyeSeparation(0.01);
    ctx.camera->setStereoFocalLength(0.5);
    ctx.camera->setMirrorVertical(ctx.mirrored);

    ctx.light = new cSpotLight(ctx.world);
    ctx.world->addChild(ctx.light);
    ctx.light->setEnabled(true);
    ctx.light->setLocalPos(0.6, 0.15, 0.3);
    ctx.light->setDir(-0.6, -0.15, -0.3);
    ctx.light->setShadowMapEnabled(true);
    ctx.light->m_shadowMap->setQualityMedium();
    ctx.light->setCutOffAngleDeg(20.0f);

    ctx.forwardPass = cRenderPass::create();
    ctx.forwardPass->setup(ctx.camera, ctx.width, ctx.height);
    ctx.fxaaGammaPass = cRenderPass::create();
    ctx.fxaaGammaPass->setup(ctx.camera, ctx.width, ctx.height);
    ctx.fxaaGammaPass->m_input = ctx.forwardPass;
    ctx.fxaaGammaPass->m_program = cShaderProgram::create(
        #include "glsl/fxaa_gamma.vert"
        ,
        #include "glsl/fxaa_gamma.frag"
    );

    for (uint32_t i = 0; i < VH_MAX_ITEMS; ++i) {
        const std::string filename = ctx.input_filenames[i];
        if (filename.find(".vtk") != std::string::npos)
            vh::loadFileVTK(&ctx.vtc.voxmaps[i], filename);
        if (filename.find(".png") != std::string::npos) {
            ctx.custom_label = filename[filename.size() - 5] - '3';  // XXX
            vh::createVoxmapFromHeightmap(&ctx.vtc.voxmaps[i], filename, i == 0);
        }

        vh::computeMinMaxVolume(&ctx.vtc.voxmaps[i], &ctx.vtc.minmax_volumes[i]);
        vh::createMaskVolume(&ctx.vtc.mask_volumes[i]);
        ctx.vtc.poses[i].pos = { 0.0f, 0.2f * i - 0.1f, 0.0f };
        ctx.vtc.bounds[i][0] = { -0.1f, -0.1f, -0.1f };
        ctx.vtc.bounds[i][1] = {  0.1f,  0.1f,  0.1f };
        ctx.vtc.isovalues[i] = 0.5f;
    }

    for (uint32_t i = 0; i < VH_MAX_ITEMS; ++i) {
        const int32_t capacity = ctx.max_dynamic_points;
        ctx.vtc.pointshells[i].data.resize(capacity * 4);
        ctx.vtc.pointshells[i].psi.resize(1);
        ctx.vtc.pointshells[i].psi[0].capacity = capacity;
        ctx.vtc.pointshells[i].psi[0].data = &ctx.vtc.pointshells[i].data[0];  // HACK
    }
    for (uint32_t i = 0; i < VH_MAX_ITEMS; ++i) {
        vh::updateStaticPointShell(ctx.vtc.voxmaps[i], ctx.vtc.minmax_volumes[i],
                                   ctx.vtc.isovalues[i] * 255.0f, &ctx.vtc.pointshells2[i]);
        vh::generatePointShellLods(&ctx.vtc.pointshells2[i]);
    }

    for (uint32_t i = 0; i < VH_MAX_ITEMS; ++i) {
        auto volume_object = new cVolumeObject();
        ctx.world->addChild(volume_object);
        volume_object->m_vtc = &ctx.vtc;
        volume_object->m_id = i;
        volume_object->m_label = (ctx.custom_label >= 0) ? ctx.custom_label : i;
        volume_object->m_show_label = i != 0;
        volume_object->computeBoundaryBox(false);
        ctx.volume_objects[i] = volume_object;
    }
}

void initHapticsDevice(Context &ctx)
{
    ctx.handler = new cHapticDeviceHandler();
    ctx.handler->getDevice(ctx.hapticDevice, 0);
    ctx.hapticDevice->open();
    ctx.hapticDevice->calibrate();
}

void initApp(Context &ctx)
{
    initHapticsDevice(ctx);
    initScene(ctx);

    // Set up the haptics rendering thread
    ctx.hapticsThread = new cThread();
    ctx.hapticsThread->start(updateHaptics, CTHREAD_PRIORITY_HAPTICS, &ctx);
}

void shutdownApp(Context &ctx)
{
    ctx.simulationRunning = false;

    // Wait for haptics thread to terminate
    while (!ctx.simulationFinished) { cSleepMs(100); }
    ctx.hapticDevice->close();

    delete ctx.hapticsThread;
    delete ctx.world;
    delete ctx.handler;
}

void updateGui(Context &ctx)
{
    ImGui::SetNextWindowCollapsed(true, ImGuiSetCond_Once);
    if (ImGui::Begin("Settings")) {
        if (ImGui::CollapsingHeader("Haptics")) {
            ImGui::Checkbox("Haptics enabled", &ctx.haptics_enabled);
            ImGui::Combo("Compute mode", (int *)&ctx.compute_mode, "Scalar\0ISPC\0");
            ImGui::Combo("Pointshell type", (int *)&ctx.ps_type, "Static\0Dynamic\0");
            ImGui::SliderFloat("Max force", &ctx.max_force, 0.0f, 10.0f, "%.1f");
            ImGui::SliderInt("Max LOD", &ctx.max_point_lod, 0, 5);
            ImGui::Checkbox("Use collision mask", &ctx.use_collision_mask);
            ImGui::Checkbox("Use virtual coupling", &ctx.use_virtual_coupling);
            ImGui::Checkbox("Use force scaling", &ctx.use_force_scaling);
            ImGui::Checkbox("Use automatic LOD", &ctx.use_automatic_lod);
            ImGui::SliderInt("Solver steps", &ctx.vtc.vc.num_solver_steps, 1, 4);
        }
        if (ImGui::CollapsingHeader("Visualization")) {
            ImGui::Checkbox("Show volume object", &ctx.show_volume_object);
            ImGui::Checkbox("Show outline", &ctx.show_boundary_box);
            ImGui::Checkbox("Show isosurface", &ctx.show_isosurface);
            ImGui::Checkbox("Show proxy geometry", &ctx.show_proxy_geometry);
            ImGui::Checkbox("Show minmax blocks", &ctx.show_minmax_blocks);
            ImGui::Checkbox("Show collision grid", &ctx.show_collision_grid);
            ImGui::Checkbox("Show coarse collisions", &ctx.show_coarse_collisions);
            ImGui::Checkbox("Show pointshell", &ctx.show_pointshell);
            ImGui::Checkbox("Show point normals", &ctx.show_point_normals);
            ImGui::Checkbox("Show point IDs", &ctx.show_point_ids);
            ImGui::Checkbox("Shadows", &ctx.shadows);
        }
        if (ImGui::CollapsingHeader("Misc")) {
            ImGui::ColorEdit3("Background", &ctx.world->m_backgroundColor[0]);
            ImGui::Checkbox("FXAA", &ctx.fxaa);
            ImGui::Checkbox("Gamma correction", &ctx.gamma);
        }
    }
    ImGui::End();
    ImGui::SetNextWindowCollapsed(true, ImGuiSetCond_Once);
    if (ImGui::Begin("Performance")) {
        ImGui::Text("Graphics framerate (Hz): %.1f", ctx.freqCounterGraphics.getFrequency());
        ImGui::Text("Haptics framerate (Hz): %.1f", ctx.freqCounterHaptics.getFrequency());
        ImGui::Text("Max stiffness (N/m): %.1f", ctx.maxStiffness);
        ImGui::Text("Output force (N): %.1f %.1f %.1f", ctx.ft.force[0], ctx.ft.force[1], ctx.ft.force[2]);
        ImGui::Text("Output torque (Nm): %.1f %.1f %.1f", ctx.ft.torque[0], ctx.ft.torque[1], ctx.ft.torque[2]);
        ImGui::Text("Point queries (count): %d", ctx.num_point_queries);
        ImGui::Text("Contacts (count): %d", ctx.ft.count);
        ImGui::Text("Distance (voxels): %f", (256.0f / 0.2f) *vh::length(
            vh::sub(ctx.vtc.poses[0].pos, ctx.vtc.poses[1].pos)));
        ImGui::Text("Timings (s):");
        ImGui::PlotLines("Coarse collision", &ctx.timings[0].data[ctx.timings[0].first],
                         ctx.timings[0].last - ctx.timings[0].first, 0, 0, 0.0f, 1e-3f);
        ImGui::PlotLines("Poinsthell update", &ctx.timings[1].data[ctx.timings[1].first],
                         ctx.timings[1].last - ctx.timings[1].first, 0, 0, 0.0f, 1e-3f);
        ImGui::PlotLines("Fine collision", &ctx.timings[2].data[ctx.timings[2].first],
                         ctx.timings[2].last - ctx.timings[2].first, 0, 0, 0.0f, 1e-3f);
    }
    ImGui::End();
}

void updateSimulation(Context &ctx)
{
    vh::ForceTorque ft;
    if (!ctx.haptics_enabled) { ctx.ft = ft; return; }

    double tic0 = glfwGetTime();
    {
        auto &a_minmax_vol = ctx.vtc.minmax_volumes[0];
        auto &a_pose = ctx.vtc.poses[0];
        auto &b_minmax_vol = ctx.vtc.minmax_volumes[1];
        auto &b_pose = ctx.vtc.poses[1];
        auto &b_mask_vol = ctx.vtc.mask_volumes[1];

        if (ctx.compute_mode == vh::VH_COMPUTE_MODE_SCALAR) {
            vh::intersectMinMaxVolumes(a_minmax_vol, a_pose, b_minmax_vol, b_pose, &b_mask_vol);
        }
        if (ctx.compute_mode == vh::VH_COMPUTE_MODE_ISPC) {
            ispc::VolumeInfo a_minmax_vol_ = { (int *)a_minmax_vol.dimensions, &a_minmax_vol.data[0] };
            ispc::VolumeInfo b_minmax_vol_ = { (int *)b_minmax_vol.dimensions, &b_minmax_vol.data[0] };
            ispc::VolumeInfo b_mask_vol_ = { (int *)b_mask_vol.dimensions, &b_mask_vol.data[0] };
            ispc::intersectMinMaxVolumes(a_minmax_vol_, (ispc::RigidTransform &)a_pose,
                                         b_minmax_vol_, (ispc::RigidTransform &)b_pose,
                                         b_mask_vol_);
        }
    }
    vh::addTiming(ctx.timings[0], float(glfwGetTime() - tic0));

    double tic1 = glfwGetTime();
    if (ctx.ps_type == vh::VH_POINTSHELL_TYPE_DYNAMIC) {
        for (uint32_t i = 0; i < VH_MAX_ITEMS; ++i) {
            auto &voxmap = ctx.vtc.voxmaps[i];
            auto &minmax_vol = ctx.vtc.minmax_volumes[i];
            auto &mask_vol = ctx.vtc.mask_volumes[i];
            auto &ps = ctx.vtc.pointshells[i].psi[0];
            const bool use_collision_mask = ctx.use_collision_mask;
            const float isovalue = ctx.vtc.isovalues[i] * 255.0f;

            if (ctx.compute_mode == vh::VH_COMPUTE_MODE_SCALAR) {
                vh::updateDynamicPointShell(voxmap, minmax_vol, mask_vol, use_collision_mask, isovalue, &ps);
            }
            if (ctx.compute_mode == vh::VH_COMPUTE_MODE_ISPC) {
                ispc::VolumeInfo voxmap_ = { (int *)voxmap.dimensions, &voxmap.data[0] };
                ispc::VolumeInfo minmax_vol_ = { (int *)minmax_vol.dimensions, &minmax_vol.data[0] };
                ispc::VolumeInfo mask_vol_ = { (int *)mask_vol.dimensions, &mask_vol.data[0] };
                ispc::updateDynamicPointShell(voxmap_, minmax_vol_, mask_vol_, use_collision_mask,
                                              isovalue, (ispc::PointShellInfo &)ps);
            }
        }
    }
    vh::addTiming(ctx.timings[1], float(glfwGetTime() - tic1));

    double tic2 = glfwGetTime();
    {
        auto &a_voxmap = ctx.vtc.voxmaps[0];
        auto &a_pose = ctx.vtc.poses[0];
        auto &b_ps = ctx.vtc.pointshells2[1];
        auto &b_psi = ctx.vtc.pointshells[1].psi[0];
        auto &b_pose = ctx.vtc.poses[1];
        auto &b_mask_vol = ctx.vtc.mask_volumes[1];
        static std::vector<int32_t> b_collision_blocks(4096);  // HACK

        uint32_t num_point_queries = 0;
        if (ctx.ps_type == vh::VH_POINTSHELL_TYPE_DYNAMIC) {
            num_point_queries += b_psi.count;
            if (ctx.compute_mode == vh::VH_COMPUTE_MODE_SCALAR) {
                vh::computeForceTorque(a_voxmap, a_pose, b_psi, b_pose, &ft);
            }
            if (ctx.compute_mode == vh::VH_COMPUTE_MODE_ISPC) {
                ispc::VolumeInfo a_voxmap_ = { (int *)a_voxmap.dimensions, &a_voxmap.data[0] };
                ispc::computeForceTorque(a_voxmap_, (ispc::RigidTransform &)a_pose,
                                         (ispc::PointShellInfo &)b_psi, (ispc::RigidTransform &)b_pose,
                                         &ft.force[0], &ft.dFdx[0][0], (int *)&ft.count);
            }
        }
        if (ctx.ps_type == vh::VH_POINTSHELL_TYPE_STATIC) {
            b_collision_blocks.clear();
            for (int32_t i = 0; i < 4096; ++i)
                if (b_mask_vol.data[i] > 0) b_collision_blocks.push_back(i);

            auto &buffer = ctx.vtc.pointshells[0].psi[0];
            buffer.count = 0;
            buffer.first = 0;

            for (int32_t lod = 0; lod <= ctx.max_point_lod; ++lod) {
                for (int32_t i : b_collision_blocks) {
                    auto psi = b_ps.psi[i];
                    auto lodinfo = b_ps.lodinfo[i];
                    psi.first += lodinfo.first[lod];
                    psi.count = lodinfo.count[lod];
                    num_point_queries += psi.count;

                    if (ctx.compute_mode == vh::VH_COMPUTE_MODE_SCALAR) {
                        vh::computeForceTorque(a_voxmap, a_pose, psi, b_pose, &ft);
                    }
                    if (lod > 2 && ctx.compute_mode == vh::VH_COMPUTE_MODE_ISPC) {
                        ispc::VolumeInfo a_voxmap_ = { (int *)a_voxmap.dimensions, &a_voxmap.data[0] };
                        ispc::computeForceTorque(a_voxmap_, (ispc::RigidTransform &)a_pose,
                                                 (ispc::PointShellInfo &)psi, (ispc::RigidTransform &)b_pose,
                                                 &ft.force[0], &ft.dFdx[0][0], (int *)&ft.count);
                    }
                    if (lod <= 2 && ctx.compute_mode == vh::VH_COMPUTE_MODE_ISPC) {
                        std::memcpy(&buffer.data[4 * buffer.count],
                                    &psi.data[4 * psi.first], psi.count * 16);
                        buffer.count += psi.count;

                        if (buffer.count > 256) {
                            ispc::VolumeInfo a_voxmap_ = {(int *)a_voxmap.dimensions,&a_voxmap.data[0]};
                            ispc::computeForceTorque(a_voxmap_, (ispc::RigidTransform &)a_pose,
                                         (ispc::PointShellInfo &)buffer, (ispc::RigidTransform &)b_pose,
                                         &ft.force[0], &ft.dFdx[0][0], (int *)&ft.count);
                            buffer.count = 0;
                        }
                    }
                }
            }
            ispc::VolumeInfo a_voxmap_ = {(int *)a_voxmap.dimensions,&a_voxmap.data[0]};
            ispc::computeForceTorque(a_voxmap_, (ispc::RigidTransform &)a_pose,
                         (ispc::PointShellInfo &)buffer, (ispc::RigidTransform &)b_pose,
                         &ft.force[0], &ft.dFdx[0][0], (int *)&ft.count);
        }
        ctx.num_point_queries = num_point_queries;
    }
    double toc2 = glfwGetTime();
    vh::addTiming(ctx.timings[2], float(glfwGetTime() - tic2));

    if (ctx.ps_type == vh::VH_POINTSHELL_TYPE_STATIC && ctx.use_automatic_lod) {
        const double lower_threshold = 1e-4;
        const double upper_threshold = 6e-4;
        if ((toc2 - tic2) < lower_threshold)
            ctx.max_point_lod += 1;
        if ((toc2 - tic2) > upper_threshold)
            ctx.max_point_lod -= 1;
        ctx.max_point_lod = std::max(0, std::min(VH_POINTSHELL_MAX_LOD, ctx.max_point_lod));
    }

    ctx.ft = ft;
}

void updateGraphics(Context &ctx)
{
    ctx.light->setShadowMapEnabled(ctx.shadows);

    cMatrix3d matrix;
    cTrackballGetMatrix3d(&ctx.trackball, &matrix(0, 0));
    vh::Mat3 rot;
    for (uint32_t i = 0; i < 9; ++i)
        rot[i % 3][i / 3] = float(matrix(i / 3, i % 3));

    for (uint32_t i = 0; i < VH_MAX_ITEMS; ++i) {
        ctx.vtc.poses[i].rot = rot;
        ctx.volume_objects[i]->setShowEnabled(ctx.show_volume_object);
        ctx.volume_objects[i]->setShowBoundaryBox(ctx.show_boundary_box);
        ctx.volume_objects[i]->m_show_isosurface = ctx.show_isosurface;
        ctx.volume_objects[i]->m_show_proxy_geometry = ctx.show_proxy_geometry;
        ctx.volume_objects[i]->m_show_minmax_blocks = ctx.show_minmax_blocks;
        ctx.volume_objects[i]->m_show_collision_grid = ctx.show_collision_grid;
        ctx.volume_objects[i]->m_show_coarse_collisions = ctx.show_coarse_collisions;
        ctx.volume_objects[i]->m_show_pointshell = ctx.show_pointshell;
        ctx.volume_objects[i]->m_show_point_normals = ctx.show_point_normals;
        ctx.volume_objects[i]->m_show_point_ids = ctx.show_point_ids;
        ctx.volume_objects[i]->m_max_point_lod = ctx.max_point_lod;
        ctx.volume_objects[i]->m_pointshell_type = ctx.ps_type;
        ctx.volume_objects[i]->setLocalRot(matrix);
        const auto &pos = ctx.vtc.poses[i].pos;
        ctx.volume_objects[i]->setLocalPos(pos.x, pos.y, pos.z);
    }
    if (ctx.mouse_transform.moving)
        ctx.vtc.poses[1].pos = ctx.mouse_transform.pos[1];

#if UPDATE_SIMULATION_IN_GFX_THREAD
    // This is only for debugging
    updateSimulation(ctx);
#endif

    // Render world
    ctx.world->updateShadowMaps(false, ctx.mirrored);
    ctx.forwardPass->renderView();
    ctx.fxaaGammaPass->render();
    if (ctx.fxaa) ctx.fxaaGammaPass->blit(); else ctx.forwardPass->blit();

    // Update graphics frame counter
    ctx.freqCounterGraphics.signal(1);
}

void updateHaptics(void *userdata)
{
    Context *ctx = static_cast<Context *>(userdata);

    ctx->simulationRunning  = true;
    ctx->simulationFinished = false;
    bool grab_state = false;
    vh::Vec3 grab_vector;

    while (ctx->simulationRunning) {
        ctx->world->computeGlobalPositions(true);
        ctx->cursor->updateFromDevice();

        if (ctx->cursor->getUserSwitch(0)) {
            vh::Vec3 device_pos = {
                (float)ctx->cursor->getDeviceGlobalPos().x(),
                (float)ctx->cursor->getDeviceGlobalPos().y(),
                (float)ctx->cursor->getDeviceGlobalPos().z() };
            if (!grab_state)
                grab_vector = vh::sub(ctx->vtc.poses[1].pos, device_pos);
            ctx->vtc.vc.device_pose.pos = add(device_pos, grab_vector);
            if (!grab_state || !ctx->use_virtual_coupling) {
                ctx->vtc.vc.virtual_pose = ctx->vtc.vc.device_pose;
            }
            ctx->vtc.poses[1].pos = ctx->vtc.vc.virtual_pose.pos;
        }
        grab_state = ctx->cursor->getUserSwitch(0);

    #if !UPDATE_SIMULATION_IN_GFX_THREAD
        updateSimulation(*ctx);
        if (!ctx->use_virtual_coupling) {
            if (ctx->use_force_scaling) {
                float force_scaling = ctx->ft.count > 10 ? 10.0f / ctx->ft.count : 1.0f;
                ctx->ft.force = vh::mul(ctx->ft.force, force_scaling);
            }
        }
        if (ctx->use_virtual_coupling) {
            if (ctx->use_force_scaling) {
                float force_scaling = ctx->ft.count > 100 ? 100.0f / ctx->ft.count : 1.0f;
                ctx->ft.force = vh::mul(ctx->ft.force, force_scaling);
                ctx->ft.dFdx[0] = vh::mul(ctx->ft.dFdx[0], force_scaling);
                ctx->ft.dFdx[1] = vh::mul(ctx->ft.dFdx[1], force_scaling);
                ctx->ft.dFdx[2] = vh::mul(ctx->ft.dFdx[2], force_scaling);
            }
            vh::solveVirtualCoupling(ctx->ft, &ctx->vtc.vc);
            ctx->ft.force = ctx->vtc.vc.virtual_force;
        }
        ctx->ft.force = vh::vmax(vh::vmin(ctx->ft.force, ctx->max_force), -ctx->max_force);
    #endif

        ctx->cursor->computeInteractionForces();
        if (ctx->cursor->getUserSwitch(0))
            ctx->cursor->setDeviceGlobalForce(ctx->ft.force.x, ctx->ft.force.y, ctx->ft.force.z);
        ctx->cursor->applyToDevice();

        // Update haptic frame counter
        ctx->freqCounterHaptics.signal(1);
    }

    ctx->simulationFinished = true;
}

void printPoseInfo(Context &ctx)
{
    // Output local pose difference in voxels
    vh::Vec3 global_diff = vh::sub(ctx.vtc.poses[1].pos, ctx.vtc.poses[0].pos);
    vh::Vec3 local_diff = vh::mul(vh::transpose(ctx.vtc.poses[1].rot), global_diff);
    local_diff = vh::mul(local_diff, 256.0f / 0.2f);
    std::fprintf(stdout, "%d %d %d %.3f %.3f %.3f\n", ctx.ps_type, ctx.compute_mode,
                 ctx.max_point_lod, local_diff[0], local_diff[1], local_diff[2]);
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    ImGui_ImplGlfwGL3_KeyCallback(window, key, scancode, action, mods);
    if (ImGui::GetIO().WantCaptureKeyboard) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE) {
        printPoseInfo(*ctx);
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    if (action == GLFW_PRESS && key == GLFW_KEY_F) {
        ctx->fullscreen = !ctx->fullscreen;

        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        static int w, h, x, y;  // Used for restoring previous window pos/size
        if (ctx->fullscreen) {
            glfwGetWindowSize(window, &w, &h);
            glfwGetWindowPos(window, &x, &y);
            glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
            glfwSwapInterval(ctx->swap_interval);
            glfwGetWindowSize(ctx->window, &ctx->width, &ctx->height);
        }
        else {
            glfwSetWindowMonitor(window, nullptr, x, y, w, h, mode->refreshRate);
            glfwSwapInterval(ctx->swap_interval);
        }
    }
    if (action == GLFW_PRESS && key == GLFW_KEY_S) {
        ctx->swap_interval = ctx->swap_interval ? 0 : 1;
        glfwSwapInterval(ctx->swap_interval);
    }
    if (action == GLFW_PRESS && key == GLFW_KEY_M) {
        ctx->mirrored = !ctx->mirrored;
        ctx->camera->setMirrorVertical(ctx->mirrored);
    }
    if (action == GLFW_PRESS && key == GLFW_KEY_T) {
        vh::saveFileCSV(ctx->timings, 3, "timings.csv");
    }
}

void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
    ImGui_ImplGlfwGL3_MouseButtonCallback(window, button, action, mods);
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        double x, y;
        glfwGetCursorPos(window, &x, &y);
        ctx->mouse_transform.moving = (action == GLFW_PRESS);
        ctx->mouse_transform.center[0] = x;
        ctx->mouse_transform.center[1] = y;
        ctx->mouse_transform.pos[0] = ctx->vtc.poses[1].pos;
        ctx->mouse_transform.pos[1] = ctx->mouse_transform.pos[0];
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        double x, y;
        glfwGetCursorPos(window, &x, &y);
        ctx->trackball.center[0] = x;
        ctx->trackball.center[1] = y;
        ctx->trackball.tracking = (action == GLFW_PRESS);
    }
}

void cursorPosCallback(GLFWwindow* window, double x, double y)
{
    if (ImGui::GetIO().WantCaptureMouse) { return; }  // Skip other handling

    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    if (ctx->mouse_transform.moving) {
        ctx->mouse_transform.pos[1] = ctx->mouse_transform.pos[0];
        ctx->mouse_transform.pos[1].y += float(x - ctx->mouse_transform.center[0]) * 5e-4f;
        ctx->mouse_transform.pos[1].z -= float(y - ctx->mouse_transform.center[1]) * 5e-4f;
    }
    cTrackballMove(&ctx->trackball, x, y);
}

void windowSizeCallback(GLFWwindow* window, int width, int height)
{
    Context *ctx = static_cast<Context *>(glfwGetWindowUserPointer(window));

    ctx->width = width;
    ctx->height = height;
    ctx->forwardPass->setSize(ctx->width, ctx->height);
    ctx->fxaaGammaPass->setSize(ctx->width, ctx->height);
}

void errorCallback(int err, const char* description)
{
    std::fprintf(stderr, "GLFW error: %s\n", description);
}

int main(int argc, char* argv[])
{
    Context ctx;
    ctx.input_filenames[0] = "data/bunny_256_opacity_uint8.vtk";
    ctx.input_filenames[1] = "data/dragon_256_opacity_uint8.vtk";
    if (argc > 2) {
        ctx.input_filenames[0] = std::string(argv[1]);
        ctx.input_filenames[1] = std::string(argv[2]);
        if (argc > 3)
            (int &)ctx.ps_type = std::atoi(argv[3]);
        if (argc > 4)
            (int &)ctx.compute_mode = std::atoi(argv[4]);
        if (argc > 5)
            ctx.max_dynamic_points = std::atoi(argv[5]);
    }

    glfwSetErrorCallback(errorCallback);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_STEREO, ctx.stereo_mode == C_STEREO_ACTIVE ? GL_TRUE : GL_FALSE);
    ctx.window = glfwCreateWindow(1000, 700, "vector_haptics", 0, 0);
    if (!ctx.window) {
        std::fprintf(stderr, "Failed to create GLFW window\n");
        std::exit(EXIT_FAILURE);
    }
    glfwGetWindowSize(ctx.window, &ctx.width, &ctx.height);
    glfwSetKeyCallback(ctx.window, keyCallback);
    glfwSetMouseButtonCallback(ctx.window, mouseButtonCallback);
    glfwSetCursorPosCallback(ctx.window, cursorPosCallback);
    glfwSetWindowSizeCallback(ctx.window, windowSizeCallback);
    glfwSetWindowUserPointer(ctx.window, &ctx);
    glfwMakeContextCurrent(ctx.window);
    glfwSwapInterval(ctx.swap_interval);

    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        std::fprintf(stderr, "Failed to initialize GLEW library\n");
        std::exit(EXIT_FAILURE);
    }
    //std::fprintf(stdout, "GL version: %s\n", glGetString(GL_VERSION));

    // Initialize ImGUI without installing default callbacks
    ImGui_ImplGlfwGL3_Init(ctx.window, false);

    initApp(ctx);
    while (!glfwWindowShouldClose(ctx.window)) {
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();
        updateGraphics(ctx);
        updateGui(ctx);
        ImGui::Render();
        glfwSwapBuffers(ctx.window);
    }
    shutdownApp(ctx);

    glfwDestroyWindow(ctx.window);
    glfwTerminate();
    std::exit(EXIT_SUCCESS);
}
