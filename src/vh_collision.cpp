/// @file    vh_collision.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vh_collision.h"
#include "vh_volume.h"
#include "vh_pointshell.h"

#include <stddef.h>  // Needed for size_t
#include <cassert>

namespace vh {

static const Vec3 g_cube[] = {
    { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f, 0.0f },
    { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f }
};

void intersectMinMaxVolumes(const Volume &a_minmax_vol, const RigidTransform &a_pose,
                            const Volume &b_minmax_vol, const RigidTransform &b_pose, Volume *b_mask_vol)
{
    assert(b_mask_vol != nullptr);
    assert(a_minmax_vol.dimensions[0] == b_minmax_vol.dimensions[0]);
    assert(a_minmax_vol.dimensions[1] == b_minmax_vol.dimensions[1]);
    assert(a_minmax_vol.dimensions[2] == b_minmax_vol.dimensions[2]);

    // TODO Replace hardcoded values
    const uint32_t *dim = &a_minmax_vol.dimensions[0];
    const uint8_t isovalue = 127;  // TODO
    const float a_scale = 0.2f / dim[0];
    const float a_shift = -0.1f;
    const float b_scale = 0.2f / dim[0];
    const float b_shift = -0.1f;

    // Pre-compute AABB of cube corners transformed into A's local space
    AABB aabb_cube = { 9999.0f, 9999.0f, 9999.0f, -9999.0f, -9999.0f, -9999.0f };
    for (uint32_t i = 0; i < 8; ++i) {
        Vec3 p = mul(g_cube[i], b_scale);
        Vec3 q = mul(transpose(a_pose.rot), mul(b_pose.rot, p));
        aabb_cube[0] = vmin(aabb_cube[0], q);
        aabb_cube[1] = vmax(aabb_cube[1], q);
    }

    for (uint32_t k = 0; k < dim[2]; ++k) {
        for (uint32_t j = 0; j < dim[1]; ++j) {
            for (uint32_t i = 0; i < dim[0]; ++i) {
                // Clear B-voxel's collision mask
                b_mask_vol->data[k * dim[0] * dim[1] + j * dim[0] + i] = 0;

                // Check that B-voxel contains an isosurface
                uint8_t b_min = b_minmax_vol.data[(k * dim[0] * dim[1] + j * dim[0] + i) * 2 + 0];
                uint8_t b_max = b_minmax_vol.data[(k * dim[0] * dim[1] + j * dim[0] + i) * 2 + 1];
                if (b_min > isovalue || b_max <= isovalue) continue;

                // Compute AABB of B-voxel transformed into A's voxel space
                Vec3 p = add(mul({ float(i), float(j), float(k) }, b_scale), b_shift);
                Vec3 q = mul(transpose(a_pose.rot), sub(add(mul(b_pose.rot, p), b_pose.pos),a_pose.pos));
                AABB aabb = { add(q, aabb_cube[0]), add(q, aabb_cube[1]) };
                aabb[0] = vmax(floor(div(sub(aabb[0], a_shift), a_scale)), 0.0f);
                aabb[1] = vmin(ceil(div(sub(aabb[1], a_shift), a_scale)), dim[0]);

                // Obtain max value of A inside AABB
                uint8_t a_max = 0;
                for (uint32_t z = aabb[0].z; z < aabb[1].z; ++z)
                    for (uint32_t y = aabb[0].y; y < aabb[1].y; ++y)
                        for (uint32_t x = aabb[0].x; x < aabb[1].x; ++x)
                            a_max = smax(a_max, a_minmax_vol.data[(z * dim[0] * dim[1] + y * dim[0] + x) * 2 + 1]);

                // Update B-voxel's collision mask
                b_mask_vol->data[k * dim[0] * dim[1] + j * dim[0] + i] = uint8_t(a_max > 127);
            }
        }
    }
}

static float mix(float a, float b, float t)
{
    return (1.0f - t) * a + t * b;
}

static float cellLinear(const Vec3 &p, const uint8_t c[8])
{
    return mix(mix(mix(c[0], c[1], p.x), mix(c[2], c[3], p.x), p.y),
               mix(mix(c[4], c[5], p.x), mix(c[6], c[7], p.x), p.y), p.z);
}

void computeForceTorque(const Volume &a_voxmap, const RigidTransform &a_pose,
                        const PointShellInfo &b_ps, const RigidTransform &b_pose, ForceTorque *ft)
{
    assert(ft != nullptr);

    // TODO Replace hardcoded values
    const uint32_t *dim = &a_voxmap.dimensions[0];
    const float isovalue = 127.0f;
    const float stiffness = 800.0f;
    const float a_scale = 0.2f / dim[0];
    const float a_shift = -0.1f;
    const float b_scale = 0.2f / 16.0f;
    const float b_shift = -0.1f;

    ForceTorque accum;

    for (uint32_t i = 0; i < b_ps.count; ++i) {
        const uint32_t j = (b_ps.first + i) % b_ps.capacity;
        const float *point = &b_ps.data[j * 4];

        // Transform B-point into A's voxel space
        Vec3 p = add(mul({ point[0], point[1], point[2] }, b_scale), b_shift);
        Vec3 q = mul(transpose(a_pose.rot), sub(add(mul(b_pose.rot, p), b_pose.pos), a_pose.pos));
        q = div(sub(q, a_shift), a_scale);

        // Check that transformed point is inside A's bounds
        if (q.x < 0 || q.x >= (dim[0] - 1) ||
            q.y < 0 || q.y >= (dim[1] - 1) ||
            q.z < 0 || q.z >= (dim[2] - 1)) continue;

        // Fetch corner values of closest grid cell in A
        const size_t ofs = size_t(q.z) * dim[0] * dim[1] + int(q.y) * dim[0] + int(q.x);
        uint8_t c[8];
        c[0] = a_voxmap.data[ofs + 0];
        c[1] = a_voxmap.data[ofs + 1];
        c[2] = a_voxmap.data[ofs + dim[0] + 0];
        c[3] = a_voxmap.data[ofs + dim[0] + 1];
        c[4] = a_voxmap.data[ofs + dim[0] * dim[1] + 0];
        c[5] = a_voxmap.data[ofs + dim[0] * dim[1] + 1];
        c[6] = a_voxmap.data[ofs + dim[0] * dim[1] + dim[0] + 0];
        c[7] = a_voxmap.data[ofs + dim[0] * dim[1] + dim[0] + 1];

        // Compute force and torque contribution
        float value = cellLinear(fract(q), c) * (1.0f / 255.0f);
        if (value > 0.0f) {
            Vec3 normal = {
                float(((int &)point[3] >> 0) & 255) * (1.0f / 127.5f) - 1.0f,
                float(((int &)point[3] >> 8) & 255) * (1.0f / 127.5f) - 1.0f,
                float(((int &)point[3] >> 16) & 255) * (1.0f / 127.5f) - 1.0f };
            Vec3 force = mul(mul(b_pose.rot, normal), -value * a_scale * stiffness);
            Mat3 n_outer_n = outerProduct(mul(b_pose.rot, normal));

            accum.force = add(accum.force, force);
            accum.dFdx[0] = add(accum.dFdx[0], mul(n_outer_n[0], -stiffness));
            accum.dFdx[1] = add(accum.dFdx[1], mul(n_outer_n[1], -stiffness));
            accum.dFdx[2] = add(accum.dFdx[2], mul(n_outer_n[2], -stiffness));
            accum.count += 1;
        }
    }

    ft->force = add(ft->force, accum.force);
    ft->torque = add(ft->torque, accum.torque);
    ft->dFdx = add(ft->dFdx, accum.dFdx);
    ft->count += accum.count;
}

} // namespace vh
