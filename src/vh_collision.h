/// @file    vh_collision.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vh_forward.h"
#include "vh_transform.h"

#include <stdint.h>

namespace vh {

struct ForceTorque {
    Vec3 force = VEC3_ZEROS;
    Vec3 torque = VEC3_ZEROS;
    Mat3 dFdx = MAT3_ZEROS;
    uint32_t count = 0;  // Number of contacts
};

// Test intersection of min-max block volume B against A, and return an
// intersection mask for B that can be used for further collision detection
void intersectMinMaxVolumes(const Volume &a_minmax_vol, const RigidTransform &a_pose,
                            const Volume &b_minmax_vol, const RigidTransform &b_pose, Volume *b_mask_vol);

// Compute contact forces and torques using the Voxmap PointShell algorithm.
// Contributions from new contacts are accumulated into the ForceTorque output
// struct, so it should be cleared first in each simulation frame.
void computeForceTorque(const Volume &a_voxmap, const RigidTransform &a_pose,
                        const PointShellInfo &b_ps, const RigidTransform &b_pose, ForceTorque *ft);

} // namespace vh
