/// @file    vh_context.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vh_volume.h"
#include "vh_pointshell.h"
#include "vh_transform.h"
#include "vh_virtual_coupling.h"

namespace vh {

#define VH_MAX_ITEMS 2

enum ComputeMode {
    VH_COMPUTE_MODE_SCALAR = 0,
    VH_COMPUTE_MODE_ISPC = 1,
};

struct VhContext {
    Volume voxmaps[VH_MAX_ITEMS];
    Volume minmax_volumes[VH_MAX_ITEMS];
    Volume mask_volumes[VH_MAX_ITEMS];
    PointShell pointshells[VH_MAX_ITEMS];
    PointShell pointshells2[VH_MAX_ITEMS];
    RigidTransform poses[VH_MAX_ITEMS];
    AABB bounds[VH_MAX_ITEMS];
    float isovalues[VH_MAX_ITEMS];
    VirtualCoupling vc;
};

} // namespace vh
