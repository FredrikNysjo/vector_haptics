/// @file    vh_forward.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

namespace vh {

// Forward declarations
struct VhContext;
struct Volume;
struct PointShell;
struct PointShellInfo;
struct RigidTransform;
struct Vec3;
struct Quat;
struct Mat3;
struct VirtualCoupling;
struct ForceTorque;

} // namespace vh
