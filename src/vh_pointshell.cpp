/// @file    vh_pointshell.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vh_pointshell.h"
#include "vh_volume.h"
#include "vh_transform.h"

#include <cassert>
#include <cstring>
#include <algorithm>

namespace vh {

static void mortonToXYZ(int index, int &x, int &y, int &z)
{
    x = 0, y = 0, z = 0;
    for (int i = 0; i < 10; ++i) {
        x |= ((index >> (3 * i + 0)) & 1) << i;
        y |= ((index >> (3 * i + 1)) & 1) << i;
        z |= ((index >> (3 * i + 2)) & 1) << i;
    }
}

static Vec3 cellGrad(const uint8_t c[8])
{
    float grad_x = float(c[1] + c[3] + c[5] + c[7]) - float(c[0] + c[2] + c[4] + c[6]);
    float grad_y = float(c[2] + c[3] + c[6] + c[7]) - float(c[0] + c[1] + c[4] + c[5]);
    float grad_z = float(c[4] + c[5] + c[6] + c[7]) - float(c[0] + c[1] + c[2] + c[3]);
    return { grad_x, grad_y, grad_z };
}

void updateDynamicPointShell(const Volume &voxmap, const Volume &minmax_vol, const Volume &mask_vol,
                             bool use_collision_mask, float isovalue, PointShellInfo *psi)
{
    assert(psi != nullptr);
    assert(psi->data != nullptr);

    const uint32_t *dim = &minmax_vol.dimensions[0];

    psi->first = std::min(psi->first, psi->capacity);
    psi->count = std::min(psi->count, psi->capacity);
    psi->frame = (psi->frame + 1) % 32768;

    for (int32_t n = 0; n < 4096; ++n) {
        int32_t i, j, k;
        mortonToXYZ(n, i, j, k);

        size_t ofs = size_t(k) * (dim[1] * dim[0]) + j * dim[0] + i;
        float v_min = minmax_vol.data[ofs * 2 + 0];
        float v_max = minmax_vol.data[ofs * 2 + 1];
        uint8_t mask = use_collision_mask ? mask_vol.data[ofs] : 1;

        if (v_min < isovalue && v_max >= isovalue && mask) {
            int32_t seed = ofs + psi->frame;
            float rnd_x = seed * 1.61803399f;  seed = (seed >> 1) ^ seed;
            float rnd_y = seed * 1.61803399f;  seed = (seed >> 1) ^ seed;
            float rnd_z = seed * 1.61803399f;

            int32_t x = (float(i) + rnd_x - std::floor(rnd_x)) * 16.0f;
            int32_t y = (float(j) + rnd_y - std::floor(rnd_y)) * 16.0f;
            int32_t z = (float(k) + rnd_z - std::floor(rnd_z)) * 16.0f;
            int32_t hx = 1;
            int32_t hy = 256;
            int32_t hz = 256 * 256;

            size_t ofs = size_t(z) * (256 * 256) + y * 256 + x;
            uint8_t c[7];
            c[0] = voxmap.data[ofs];
            c[1] = x > 0 ? voxmap.data[ofs - hx] : 0;
            c[2] = x < 255 ? voxmap.data[ofs + hx] : 0;
            c[3] = y > 0 ? voxmap.data[ofs - hy] : 0;
            c[4] = y < 255 ? voxmap.data[ofs + hy] : 0;
            c[5] = z > 0 ? voxmap.data[ofs - hz] : 0;
            c[6] = z < 255 ? voxmap.data[ofs + hz] : 0;

            uint8_t c_min = c[0], c_max = c[0];
            for (int32_t i = 1; i < 7; ++i) {
                c_min = std::min(c_min, c[i]);
                c_max = std::max(c_max, c[i]);
            }

            if (c_min < isovalue && c[0] >= isovalue) {
                Vec3 normal = normalize(sub({float(c[1]), float(c[3]), float(c[5])},
                                            {float(c[2]), float(c[4]), float(c[6])}));
                int32_t normal_lod = (
                    int32_t((normal.x * 0.5f + 0.5f) * 255.0f + 0.5f) +
                    int32_t((normal.y * 0.5f + 0.5f) * 255.0f + 0.5f) * 256 +
                    int32_t((normal.z * 0.5f + 0.5f) * 255.0f + 0.5f) * 65536);

                int32_t tail = psi->first;
                int32_t head = (tail + psi->count) % psi->capacity;
                psi->data[head * 4 + 0] = x * (16.0f / 256.0f);
                psi->data[head * 4 + 1] = y * (16.0f / 256.0f);
                psi->data[head * 4 + 2] = z * (16.0f / 256.0f);
                psi->data[head * 4 + 3] = (float &)normal_lod;

                psi->count = std::min(psi->count + 1, psi->capacity);
                if (psi->count >= psi->capacity - 1) {
                    psi->first = (psi->first + 1) % psi->capacity;
                }
            }
        }
    }
}

void updateStaticPointShell(const Volume &voxmap, const Volume &minmax_vol, float isovalue, PointShell *ps)
{
    assert(ps != nullptr);

    const uint32_t *dim = &minmax_vol.dimensions[0];

    ps->type = VH_POINTSHELL_TYPE_STATIC;
    ps->psi.resize(4096);
    ps->lodinfo.resize(4096);
    ps->data.clear();

    for (int32_t n = 0; n < 4096; ++n) {
        int32_t i, j, k;
        mortonToXYZ(n, i, j, k);

        size_t ofs = size_t(k) * (dim[1] * dim[0]) + j * dim[0] + i;
        float v_min = minmax_vol.data[ofs * 2 + 0];
        float v_max = minmax_vol.data[ofs * 2 + 1];

        auto &psi = ps->psi[ofs];
        psi.count = 0;
        psi.first = ps->data.size() / 4;
        psi.capacity = (1 << 24);

        if (v_min < isovalue && v_max >= isovalue) {
            Vec3 aabb_block[2];
            aabb_block[0] = floor(mul(add({ float(i), float(j), float(k) }, 0.0f), 256.0f / 16.0f));
            aabb_block[1] = ceil(mul(add({ float(i), float(j), float(k) }, 1.0f), 256.0f / 16.0f));

            for (int32_t z = aabb_block[0].z; z < aabb_block[1].z; ++z) {
                for (int32_t y = aabb_block[0].y; y < aabb_block[1].y; ++y) {
                    for (int32_t x = aabb_block[0].x; x < aabb_block[1].x; ++x) {
                        int32_t hx = 1;
                        int32_t hy = 256;
                        int32_t hz = 256 * 256;

                        size_t ofs = size_t(z) * (256 * 256) + int(y) * 256 + int(x);
                        uint8_t c[7];
                        c[0] = voxmap.data[ofs];
                        c[1] = x > 0 ? voxmap.data[ofs - hx] : 0;
                        c[2] = x < 255 ? voxmap.data[ofs + hx] : 0;
                        c[3] = y > 0 ? voxmap.data[ofs - hy] : 0;
                        c[4] = y < 255 ? voxmap.data[ofs + hy] : 0;
                        c[5] = z > 0 ? voxmap.data[ofs - hz] : 0;
                        c[6] = z < 255 ? voxmap.data[ofs + hz] : 0;

                        uint8_t c_min = c[0], c_max = c[0];
                        for (int32_t i = 1; i < 7; ++i) {
                            c_min = std::min(c_min, c[i]);
                            c_max = std::max(c_max, c[i]);
                        }

                        if (c_min < isovalue && c[0] >= isovalue) {
                            Vec3 normal = normalize(sub({float(c[1]), float(c[3]), float(c[5])},
                                                        {float(c[2]), float(c[4]), float(c[6])}));
                            int32_t normal_lod = (
                                int32_t((normal.x * 0.5f + 0.5f) * 255.0f + 0.5f) +
                                int32_t((normal.y * 0.5f + 0.5f) * 255.0f + 0.5f) * 256 +
                                int32_t((normal.z * 0.5f + 0.5f) * 255.0f + 0.5f) * 65536);

                            ps->data.push_back(x * (16.0f / 256.0f));
                            ps->data.push_back(y * (16.0f / 256.0f));
                            ps->data.push_back(z * (16.0f / 256.0f));
                            ps->data.push_back((float &)normal_lod);
                            psi.count += 1;
                        }
                    }
                }
            }
        }
    }

    // Update pointers used by pointshells
    for (int32_t i = 0; i < 4096; ++i) {
        ps->psi[i].data = &ps->data[0];
        ps->psi[i].lodinfo = &ps->lodinfo[i];
    }
}

void generatePointShellLods(PointShell *ps)
{
    const float golden_ratio = 0.618034f;
    float rnd = golden_ratio;

    for (auto &psi : ps->psi) {
        Quat *points = (Quat *)&psi.data[psi.first * 4];  // HACK

        // Shuffle points (in deterministic order)
        for (int32_t i = 0; i < int(psi.count); ++i) {
            rnd = (rnd + golden_ratio) - std::floor(rnd + golden_ratio);
            const int32_t j = int(rnd * psi.count);
            std::swap(points[i], points[j]);
        }

        // Label points according to their insertion level in an 32^3 octree
        // implicitly represented by an occupancy mipmap
        std::vector<uint8_t> mipmap(4096 * 2);
        std::memset(psi.lodinfo->count, 0, sizeof(psi.lodinfo->count));
        for (int32_t i = 0; i < int(psi.count); ++i) {
            int32_t lod = 0;
            int32_t lod_ofs = 0;
            for (; lod < VH_POINTSHELL_MAX_LOD; ++lod) {
                const int32_t dim = (1 << lod);  // LOD dimensions
                const Vec3 q = floor(mul(fract((Vec3 &)points[i]), float(dim)));

                const int32_t ofs = int(q.z) * (dim * dim) + int(q.y) * dim + int(q.x);
                if (!mipmap[lod_ofs + ofs]) { mipmap[lod_ofs + ofs] = 1; break; }
                lod_ofs += (dim * dim * dim);
            }
            (int &)points[i].w = (((int &)points[i].w & 0x00ffffff) | (lod << 24));
            psi.lodinfo->count[lod] += 1;
        }

        // Compute final LOD offsets
        std::memset(psi.lodinfo->first, 0, sizeof(psi.lodinfo->first));
        for (int32_t i = 1; i < VH_POINTSHELL_MAX_LOD; ++i)
            psi.lodinfo->first[i] = psi.lodinfo->first[i - 1] + psi.lodinfo->count[i - 1];

        // Sort points in ascending order according to their LOD
        auto cmp = [](Quat &a, Quat &b) { return a.w < b.w; };
        std::sort(points, points + psi.count, cmp);
        for (int32_t i = 1; i < int(psi.count); ++i)
            assert(points[i].w >= points[i - 1].w);  // Verify sorting
    }
}

} // namespace vh
