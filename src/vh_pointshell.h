/// @file    vh_pointshell.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vh_forward.h"

#include <stdint.h>
#include <vector>

namespace vh {

#define VH_POINTSHELL_MAX_LOD 5

enum PointShellType {
    VH_POINTSHELL_TYPE_STATIC = 0,
    VH_POINTSHELL_TYPE_DYNAMIC = 1,
};

struct LodInfo {
    uint32_t first[VH_POINTSHELL_MAX_LOD + 1];
    uint32_t count[VH_POINTSHELL_MAX_LOD + 1];
};

struct PointShellInfo {
    uint32_t first = 0;
    uint32_t count = 0;
    uint32_t capacity = 4096;  // Only used for dynamic PS
    uint32_t frame = 0;  // Only used for dynamic PS
    float *data = nullptr;
    LodInfo *lodinfo = nullptr;  // Only used for static PS
};

struct PointShell {
    uint32_t type = VH_POINTSHELL_TYPE_DYNAMIC;
    std::vector<PointShellInfo> psi;
    std::vector<LodInfo> lodinfo;
    std::vector<float> data;
};

// Update dynamic pointshell from voxmap and min-max volume data
void updateDynamicPointShell(const Volume &voxmap, const Volume &minmax_vol, const Volume &mask_vol,
                             bool use_collision_mask, float isovalue, PointShellInfo *psi);

// Update static pointshell from voxmap and min-max volume data
void updateStaticPointShell(const Volume &voxmap, const Volume &minmax_vol, float isovalue, PointShell *ps);

// Generate LODs for static pointshell
void generatePointShellLods(PointShell *ps);

} // namespace vh
