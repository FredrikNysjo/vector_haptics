/// @file    vh_timing.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vh_timing.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>

namespace vh {

void addTiming(TimingGraph &graph, const float t)
{
    graph.data.resize(graph.capacity * 2);

    graph.data[graph.last] = t;
    graph.data[(graph.last + graph.capacity) % graph.data.size()] = t;
    graph.last += 1;
    if (graph.last >= graph.data.size()) {
        graph.last = graph.capacity - 1;
        graph.first = 0;
    }
    if (graph.last >= graph.capacity)
        graph.first += 1;
}

bool saveFileCSV(const TimingGraph *graphs, size_t graph_count, const std::string &filename)
{
    assert(graphs != nullptr);

    FILE *stream = std::fopen(filename.c_str(), "wb+");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", filename.c_str());
        return false;
    }

    for (size_t i = graphs[0].first; i < graphs[0].last; ++i) {
        for (size_t j = 0; j < graph_count; ++j) {
            const char separator = (j + 1 < graph_count) ? ',' : '\n';
            std::fprintf(stream, "%f%c", graphs[j].data[i], separator);
        }
    }
    std::fclose(stream);

    return true;
}

} // namespace vh
