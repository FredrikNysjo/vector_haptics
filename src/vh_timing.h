/// @file    vh_timing.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include <stdint.h>
#include <vector>
#include <string>

namespace vh {

struct TimingGraph {
    uint32_t capacity = 1000;
    uint32_t first = 0;
    uint32_t last = 0;
    std::vector<float> data;
};

void addTiming(TimingGraph &graph, const float t);

bool saveFileCSV(const TimingGraph *graphs, size_t graph_count, const std::string &filename);

} // namespace vh
