/// @file    vh_transform.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include <cmath>

namespace vh {

struct Vec3 {
    float x, y, z;
    float &operator[](int i) { return (&x)[i]; }
    const float &operator[](int i) const { return (&x)[i]; }
};

struct Quat {
    float x, y, z, w;
    float &operator[](int i) { return (&x)[i]; }
    const float &operator[](int i) const { return (&x)[i]; }
};

struct Mat3 {
    Vec3 v[3];
    Vec3 &operator[](int i) { return v[i]; }
    const Vec3 &operator[](int i) const { return v[i]; }
};

typedef Vec3 AABB[2];

struct RigidTransform {
    Mat3 rot;
    Vec3 pos;
};

static Vec3 VEC3_ZEROS = { 0.0f, 0.0f, 0.0f };
static Vec3 VEC3_ONES = { 1.0f, 1.0f, 1.0f };
static Mat3 MAT3_ZEROS = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
static Mat3 MAT3_IDENTITY = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };

static inline Vec3 add(const Vec3 &a, const Vec3 &b)
{
    return { a.x + b.x, a.y + b.y, a.z + b.z };
}

static inline Vec3 add(const Vec3 &a, float b)
{
    return { a.x + b, a.y + b, a.z + b };
}

static inline Vec3 sub(const Vec3 &a, const Vec3 &b)
{
    return { a.x - b.x, a.y - b.y, a.z - b.z };
}

static inline Vec3 sub(const Vec3 &a, float b)
{
    return { a.x - b, a.y - b, a.z - b };
}

static inline Vec3 mul(const Vec3 &a, const Vec3 &b)
{
    return { a.x * b.x, a.y * b.y, a.z * b.z };
}

static inline Vec3 mul(const Vec3 &a, float b)
{
    return { a.x * b, a.y * b, a.z * b };
}

static inline Vec3 div(const Vec3 &a, const Vec3 &b)
{
    return { a.x / b.x, a.y / b.y, a.z / b.z };
}

static inline Vec3 div(const Vec3 &a, float b)
{
    return { a.x / b, a.y / b, a.z / b };
}

static inline float dot(const Vec3 &a, const Vec3 &b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

static inline float length(const Vec3 &v)
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

static inline Vec3 floor(const Vec3 &v)
{
    return { std::floor(v.x), std::floor(v.y), std::floor(v.z) };
}

static inline Vec3 ceil(const Vec3 &v)
{
    return { std::ceil(v.x), std::ceil(v.y), std::ceil(v.z) };
}

static inline Vec3 fract(const Vec3 &v)
{
    return { v.x - std::floor(v.x), v.y - std::floor(v.y), v.z - std::floor(v.z) };
}

static inline Vec3 normalize(const Vec3 &v)
{
    float v_norm = std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    return div(v, v_norm > 0.0f ? v_norm : 1.0f);
}

static inline float smin(float a, float b)
{
    return a < b ? a : b;
}

static inline float smax(float a, float b)
{
    return a > b ? a : b;
}

static inline Vec3 vmin(const Vec3 &a, const Vec3 &b)
{
    float x = a.x < b.x ? a.x : b.x;
    float y = a.y < b.y ? a.y : b.y;
    float z = a.z < b.z ? a.z : b.z;
    return { x, y, z };
}

static inline Vec3 vmin(const Vec3 &a, float b)
{
    float x = a.x < b ? a.x : b;
    float y = a.y < b ? a.y : b;
    float z = a.z < b ? a.z : b;
    return { x, y, z };
}

static inline Vec3 vmax(const Vec3 &a, const Vec3 &b)
{
    float x = a.x > b.x ? a.x : b.x;
    float y = a.y > b.y ? a.y : b.y;
    float z = a.z > b.z ? a.z : b.z;
    return { x, y, z };
}

static inline Vec3 vmax(const Vec3 &a, float b)
{
    float x = a.x > b ? a.x : b;
    float y = a.y > b ? a.y : b;
    float z = a.z > b ? a.z : b;
    return { x, y, z };
}

static inline Vec3 mul(const Mat3 &m, const Vec3 &v)
{
    float x = dot({ m[0].x, m[1].x, m[2].x }, v);
    float y = dot({ m[0].y, m[1].y, m[2].y }, v);
    float z = dot({ m[0].z, m[1].z, m[2].z }, v);
    return { x, y, z };
}

static inline Mat3 add(const Mat3 &a, const Mat3 &b)
{
    Vec3 v0 = add(a[0], b[0]);
    Vec3 v1 = add(a[1], b[1]);
    Vec3 v2 = add(a[2], b[2]);
    return { v0, v1, v2 };
}

static inline Mat3 mul(const Mat3 &a, const Mat3 &b)
{
    Vec3 v0 = mul(a, b[0]);
    Vec3 v1 = mul(a, b[1]);
    Vec3 v2 = mul(a, b[2]);
    return { v0, v1, v2 };
}

static inline Mat3 transpose(const Mat3 &m)
{
    Vec3 v0 = { m[0].x, m[1].x, m[2].x };
    Vec3 v1 = { m[0].y, m[1].y, m[2].y };
    Vec3 v2 = { m[0].z, m[1].z, m[2].z };
    return { v0, v1, v2 };
}

static inline Mat3 outerProduct(const Vec3 &v)
{
    Vec3 v0 = { v.x * v.x, v.y * v.x, v.z * v.x };
    Vec3 v1 = { v.x * v.y, v.y * v.y, v.z * v.y };
    Vec3 v2 = { v.x * v.z, v.y * v.z, v.z * v.z };
    return { v0, v1, v2 };
}

} // namespace vh
