/// @file    vh_virtual_coupling.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#include "vh_virtual_coupling.h"
#include "vh_collision.h"

#include <Eigen/Eigen>
#include <Eigen/LU>

#include <cassert>

namespace vh {

static Eigen::Matrix3f computeLHS(const VirtualCoupling &vc, const Mat3 &dFdx_collision)
{
    Eigen::Matrix3f m = (Eigen::Matrix3f &)dFdx_collision;
    m(0, 0) += -1.0f;
    m(1, 1) += -1.0f;
    m(2, 2) += -1.0f;
    return m;
}

static Eigen::Vector3f computeRHS(const Vec3 &force_collision, const Vec3 &virtual_force)
{
    Eigen::Vector3f v;
    v(0) = -force_collision.x + virtual_force.x;
    v(1) = -force_collision.y + virtual_force.y;
    v(2) = -force_collision.z + virtual_force.z;
    return v;
}

void solveVirtualCoupling(const ForceTorque &ft_collision, VirtualCoupling *vc)
{
    assert(vc != nullptr);

    for (int32_t i = 0; i < vc->num_solver_steps; ++i) {
        const RigidTransform device_pose = vc->device_pose;
        const RigidTransform virtual_pose = vc->virtual_pose;
        const Vec3 pose_diff = sub(virtual_pose.pos, device_pose.pos);
        const float k1 = dot(ft_collision.force, ft_collision.force) > 0.0f ? 1.0f : 0.01f;
        const float k2 = dot(ft_collision.force, pose_diff) > 0.0f ? 1.0f : 0.1f;
        const Vec3 virtual_force = mul(pose_diff, vc->stiffness * k1 * k2);

        // Solve for positional and rotational deltas
        const Eigen::Matrix3f m = computeLHS(*vc, ft_collision.dFdx);
        const Eigen::Vector3f y = computeRHS(ft_collision.force, virtual_force);
        const Eigen::Vector3f x = m.fullPivLu().solve(y);

        // Update pose of virtual coupling
        const float a = dot(ft_collision.force, ft_collision.force) > 0.0f ? 1e-3f : 5e-3f;
        const Vec3 delta_pos = vmax(vmin((Vec3 &)x, a), -a);
        vc->virtual_pose.pos = add(vc->virtual_pose.pos, mul(delta_pos, 0.5f));

        // Compute output force
        vc->virtual_force = VEC3_ZEROS;
        if (dot(ft_collision.force, ft_collision.force) > 0.0f)
            vc->virtual_force = mul(pose_diff, vc->stiffness * k1 * k2);
    }
}

} // namespace vh
