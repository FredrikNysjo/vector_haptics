/// @file    vh_virtual_coupling.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vh_forward.h"
#include "vh_transform.h"

namespace vh {

struct VirtualCoupling {
    RigidTransform device_pose;
    RigidTransform virtual_pose;
    Vec3 virtual_force;
    float stiffness = 800.0f;
    int num_solver_steps = 2;
};

// Solve quasi-static virtual coupling
void solveVirtualCoupling(const ForceTorque &ft_collision, VirtualCoupling *vc);

} // namespace vh
