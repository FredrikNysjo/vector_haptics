/// @file    vh_volume.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

// Notes:
// -

#include "vh_volume.h"

#include <GL/glew.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <cassert>
#include <cmath>
#include <cfloat>
#include <algorithm>

namespace vh {

void createVolumeTexture(const Volume *volume, void *texture_id)
{
    assert(volume != nullptr);
    assert(texture_id != nullptr);

    const uint32_t *dim = &volume->dimensions[0];
    const uint8_t *data = &volume->data[0];

    struct { GLenum internal, type, format; } fmt;
    fmt = { GL_R8, GL_RED, GL_UNSIGNED_BYTE };
    if (volume->format == VH_FORMAT_RG8)
        fmt = { GL_RG8, GL_RG, GL_UNSIGNED_BYTE };

    GLenum filter_mode = GL_LINEAR;
    if (volume->filter_mode == VH_FILTER_MODE_NEAREST)
        filter_mode = GL_NEAREST;

    glGenTextures(1, (GLuint *)texture_id);
    glBindTexture(GL_TEXTURE_3D, *(GLuint *)texture_id);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, filter_mode);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, filter_mode);
    glTexImage3D(GL_TEXTURE_3D, 0, fmt.internal, dim[0], dim[1], dim[2], 0, fmt.type, fmt.format, data);
    glBindTexture(GL_TEXTURE_3D, 0);
}

void computeMinMaxVolume(const Volume *src, Volume *dst, int blocks_per_side)
{
    assert(src != nullptr);
    assert(dst != nullptr);

    uint32_t *dst_dim = &dst->dimensions[0];
    const uint32_t *src_dim = &src->dimensions[0];

    dst->format = VH_FORMAT_RG8;
    dst->filter_mode = VH_FILTER_MODE_NEAREST;
    dst_dim[0] = dst_dim[1] = dst_dim[2] = blocks_per_side;
    dst->data.resize(dst_dim[0] * dst_dim[1] * dst_dim[2] * 2 * sizeof(float));

    float bw = float(src_dim[0]) / dst_dim[0];
    float bh = float(src_dim[1]) / dst_dim[1];
    float bd = float(src_dim[2]) / dst_dim[2];

    for (uint32_t k = 0; k < dst_dim[2]; ++k) {
        for (uint32_t j = 0; j < dst_dim[1]; ++j) {
            for (uint32_t i = 0; i < dst_dim[0]; ++i) {
                const float epsilon = 1e-5f;  // Small epsilon for snapping blocks to voxel boundaries
                uint32_t x_min = std::floor(bw * i + epsilon), x_max = std::ceil(bw * (i + 1) - epsilon);
                uint32_t y_min = std::floor(bh * j + epsilon), y_max = std::ceil(bh * (j + 1) - epsilon);
                uint32_t z_min = std::floor(bd * k + epsilon), z_max = std::ceil(bd * (k + 1) - epsilon);

                float v_min = FLT_MAX, v_max = -FLT_MAX;
                for (uint32_t z = z_min; z < z_max; ++z) {
                    for (uint32_t y = y_min; y < y_max; ++y) {
                        for (uint32_t x = x_min; x < x_max; ++x) {
                            size_t ofs = size_t(z) * src_dim[1] * src_dim[0] + y * src_dim[0] + x;
                            v_min = std::min(v_min, float(src->data[ofs]));
                            v_max = std::max(v_max, float(src->data[ofs]));
                        }
                    }
                }
                size_t ofs = size_t(k) * dst_dim[1] * dst_dim[0] + j * dst_dim[0] + i;
                dst->data[ofs * 2 + 0] = v_min;
                dst->data[ofs * 2 + 1] = v_max;
            }
        }
    }
}

void createMaskVolume(Volume *dst, int blocks_per_side)
{
    assert(dst != nullptr);

    uint32_t *dst_dim = &dst->dimensions[0];

    dst->format = VH_FORMAT_R8;
    dst->filter_mode = VH_FILTER_MODE_NEAREST;
    dst_dim[0] = dst_dim[1] = dst_dim[2] = blocks_per_side;
    dst->data.resize(dst_dim[0] * dst_dim[1] * dst_dim[2] * sizeof(uint8_t));
}

void createVoxmapFromHeightmap(Volume *voxmap, const std::string &filename, bool invert)
{
    assert(voxmap != nullptr);

    voxmap->format = VH_FORMAT_R8;
    voxmap->filter_mode = VH_FILTER_MODE_LINEAR;
    voxmap->dimensions[0] = 256;
    voxmap->dimensions[1] = 256;
    voxmap->dimensions[2] = 256;
    voxmap->data.resize(256 * 256 * 256);

    int32_t w, h, n;
    uint8_t *image = stbi_load(filename.c_str(), &w, &h, &n, 4);
    for (int z = 0; z < 256; ++z) {
        for (int y = 0; y < 256; ++y) {
            for (int x = 0; x < 256; ++x) {
                int ofs = z * (256 * 256) + y * 256 + x;
                uint8_t r = image[(y * w + x) * 4 + 0];
                uint8_t a = image[(y * w + x) * 4 + 3];
                if (invert)
                    voxmap->data[ofs] = r < z || a < 127 ? 0 : 255;
                else
                    voxmap->data[ofs] = r >= z || a < 127 ? 0 : 255;
            }
        }
    }
}

} // namespace vh
