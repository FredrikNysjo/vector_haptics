/// @file    vh_volume.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include <stdint.h>
#include <vector>
#include <string>

namespace vh {

enum Format {
    VH_FORMAT_UNDEFINED = 0,
    VH_FORMAT_R8,
    VH_FORMAT_RG8,
};

enum FilterMode {
    VH_FILTER_MODE_UNDEFINED = 0,
    VH_FILTER_MODE_NEAREST,
    VH_FILTER_MODE_LINEAR,
};

struct Volume {
    uint32_t format;
    uint32_t filter_mode;
    uint32_t dimensions[3];
    float spacing[3];
    float origin[3];
    std::vector<uint8_t> data;
};

void createVolumeTexture(const Volume *volume, void *texture_id);

void computeMinMaxVolume(const Volume *src, Volume *dst, int blocks_per_side=16);

void createMaskVolume(Volume *dst, int blocks_per_side=16);

void createVoxmapFromHeightmap(Volume *voxmap, const std::string &filename, bool invert=false);

} // namespace vh
