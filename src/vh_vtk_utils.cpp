/// @file    vh_vtk_utils.cpp
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

// Notes:
// - File I/O is currently limited to 8-bit unsigned volumes

#include "vh_vtk_utils.h"
#include "vh_volume.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>

namespace vh {

bool loadFileVTK(Volume *volume, const std::string &filename)
{
    assert(volume != nullptr);

    uint32_t *dim = &volume->dimensions[0];
    float *origin = &volume->origin[0];
    float *spacing = &volume->spacing[0];
    size_t num_points = 0;
    char format[256] = { 0 };

    FILE *stream = std::fopen(filename.c_str(), "rb+");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", filename.c_str());
        return false;
    }

    char line[256] = { 0 };
    while (std::fgets(line, 256, stream)) {
        size_t n;
        n = std::sscanf(line, "DIMENSIONS %d %d %d\n", &dim[0], &dim[1], &dim[2]);
        n = std::sscanf(line, "ORIGIN %f %f %f\n", &origin[0], &origin[1], &origin[2]);
        n = std::sscanf(line, "SPACING %f %f %f\n", &spacing[0], &spacing[1], &spacing[2]);
        n = std::sscanf(line, "POINT_DATA %ld\n", &num_points);
        n = std::sscanf(line, "SCALARS %*s %s", format);
        if (std::strstr(line, "LOOKUP_TABLE default\n")) {
            volume->data.resize(num_points);
            for (size_t i = 0; i < num_points; i += 4096) {
                n = std::fread(&volume->data[i], 4096, 1, stream);
            }
        }
    }
    std::fclose(stream);

    if (num_points == 0 || !std::strstr(format, "unsigned_char")) {
        std::fprintf(stderr, "Unknown VTK file\n");
        return false;
    }
    return true;
}

bool saveFileVTK(const Volume *volume, const std::string &filename)
{
    assert(volume != nullptr);

    const uint32_t *dim = &volume->dimensions[0];
    const float *origin = &volume->origin[0];
    const float *spacing = &volume->spacing[0];

    FILE *stream = std::fopen(filename.c_str(), "wb+");
    if (!stream) {
        std::fprintf(stderr, "Could not open %s\n", filename.c_str());
        return false;
    }

    std::fprintf(stream, "# vtk DataFile Version 3.0\n");
    std::fprintf(stream, "VTK File\nBINARY\nDATASET STRUCTURED_POINTS\n");
    std::fprintf(stream, "DIMENSIONS %d %d %d\n", dim[0], dim[1], dim[2]);
    std::fprintf(stream, "ORIGIN %f %f %f\n", origin[0], origin[1], origin[2]);
    std::fprintf(stream, "SPACING %f %f %f\n", spacing[0], spacing[1], spacing[2]);
    std::fprintf(stream, "POINT_DATA %ld\n", (size_t)dim[0] * dim[1] * dim[2]);
    std::fprintf(stream, "SCALARS scalars unsigned_char 1\n");
    std::fprintf(stream, "LOOKUP_TABLE default\n");
    for (size_t i = 0; i < (size_t)dim[0] * dim[1] * dim[2]; i += 4096) {
        std::fwrite(&volume->data[i], 4096, 1, stream);
    }
    std::fclose(stream);

    return true;
}

} // namespace vh
