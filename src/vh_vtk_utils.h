/// @file    vh_vtk_utils.h
/// @author  Fredrik Nysjö
///
/// @section LICENSE
///
/// Copyright (c) 2019 Fredrik Nysjö
///
/// This software may be modified and distributed under the terms
/// of the MIT license. See the LICENSE.md file for details.
///

#pragma once

#include "vh_forward.h"

#include <string>

namespace vh {

/// Loads volume image from VTK legacy format file
bool loadFileVTK(Volume *volume, const std::string &filename);

/// Saves volume image to VTK legacy format file
bool saveFileVTK(const Volume *volume, const std::string &filename);

} // namespace vh
